import { createSlice } from "@reduxjs/toolkit";

const paymentSlice = createSlice({
  name: "payment",
  initialState: {
    payment: {},
  },
  reducers: {
    setPayment: (state, action) => {
      state.payment = action.payload;
    },
  },
});

export const { setPayment } = paymentSlice.actions;
export default paymentSlice.reducer;
