import { useDispatch } from "react-redux";
import { reqPaymentById, reqUpdatePayment } from "./request";
import { setPayment } from "./paymentSlice";

const usePayment = () => {
  const dispatch = useDispatch();

  const onGetPaymentById = (id) => {
    reqPaymentById(id)
      .then((res) => {
        dispatch(setPayment(res.data.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onEditPayment = (id, payload) => {
    reqUpdatePayment(id, payload)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return {
    onGetPaymentById,
    onEditPayment,
  };
};

export default usePayment;
