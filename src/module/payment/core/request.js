import axios from "axios";

const reqPaymentById = (id) => {
  return axios.get(`api/payment/${id}`);
};

const reqUpdatePayment = (id, payload = {}) => {
  return axios.put(`api/payment/${id}`, payload);
};

export { reqPaymentById, reqUpdatePayment };
