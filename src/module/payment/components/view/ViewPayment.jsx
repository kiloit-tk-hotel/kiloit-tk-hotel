import React, { useEffect } from 'react'
import usePayment from '../../core/hook'
import { Link, useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Card, Dropdown } from "flowbite-react";


const ViewPayment = () => {
  const { id } = useParams()

  const { onGetPaymentById } = usePayment()
  const { payment } = useSelector((state) => state.payment)
  console.log(payment)

  useEffect(() => {
    onGetPaymentById(id)
  }, [id])
  return (
    <>
      <div className='p-5'>
        <Card className="max-w-lg">
          <div className="flex justify-between px-4 pt-4">
            <div className="flex flex-col gap-3  pb-10">
              <p> Payment ID :{
                payment.id
              }</p>
              <p>Total Payment : {
                payment.totalPayment
              }</p>

              <p>Payment Status : {payment.paymentStatus}</p>
              <p>Payment Date : {payment.paymentDate ? <>{new Date(payment.paymentDate).toLocaleDateString("en-CA")}</> : <>No Payment Date.</>}</p>
              <p>Payment Remark : {payment.remark ? <>{payment.remark}</> : <>This payment is not remark yet.</>}</p>
              <p>Payment Method : {payment.paymentMethod ? <>{payment.paymentMethod}</> : <>This payment don't have any method.</>}</p>

            </div>
            <div>
              <Dropdown inline label="">
                <Dropdown.Item>
                  <Link
                    to={`/edit/payment/${payment.id}`}
                    className="block  text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-200 dark:hover:bg-gray-600 dark:hover:text-white"
                  >
                    Edit
                  </Link>
                </Dropdown.Item>
              </Dropdown>
            </div>
          </div>

        </Card>
      </div>

    </>

  )
}

export default ViewPayment