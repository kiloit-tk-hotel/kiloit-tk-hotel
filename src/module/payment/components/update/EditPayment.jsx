import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import usePayment from '../../core/hook'
import { useFormik } from 'formik'
import { useSelector } from 'react-redux'
import { Button, Label, TextInput } from 'flowbite-react'
import dateFormat from '../../../helper/dateFormat'


const EditPayment = () => {
  const { id } = useParams()
  const { onGetPaymentById, onEditPayment } = usePayment()
  const payment = useSelector((state) => state.payment.payment)
  console.log(payment.paymentDate)

  useEffect(() => {
    onGetPaymentById(id)
  }, [id])

  const formik = useFormik({
    initialValues: {
      totalPayment: payment?.totalPayment || '',
      paymentStatus: payment?.paymentStatus || '',
      paymentDate: payment?.paymentDate || '',
      remark: payment?.remark || '',
      paymentMethod: payment?.paymentMethod || '',
    },
    enableReinitialize: true,
    onSubmit: (values) => {
      onEditPayment(id, values)
    },
  })


  const { formatDate } = dateFormat()

  // Check if payment data is loaded
  if (!payment) {
    return <div>Loading...</div>
  }

  return (
    <div className='p-5'>
      <h1 className='text-xl'>Edit Payment</h1>

      <form onSubmit={formik.handleSubmit}>
        <div className='grid gap-5 p-5'>

          <div>
            <div className="mb-2 block">
              <Label value="Total Payment" />
            </div>
            <TextInput name="totalPayment" type="text" value={formik.values.totalPayment}
              onChange={formik.handleChange} required />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Payment Status" />
            </div>
            <TextInput name="paymentStatus" type="text" value={formik.values.paymentStatus}
              onChange={formik.handleChange} required />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Payment Date" />
            </div>
            <TextInput name="paymentDate" type="date" value={formik.values.paymentDate ? formatDate(formik.values.paymentDate) : ''}
              onChange={formik.handleChange} required />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Remark" />
            </div>
            <TextInput name="remark" type="text" value={formik.values.remark}
              onChange={formik.handleChange} required />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Payment Method" />
            </div>
            <TextInput name="paymentMethod" type="text" value={formik.values.paymentMethod}
              onChange={formik.handleChange} required />
          </div>
        </div>

        <div className='p-5'>
          <Button type="submit">Submit</Button>
        </div>
      </form>

    </div>
  )
}

export default EditPayment
