import React, { useEffect } from 'react';
import useRole from '../core/hook';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useFormik } from 'formik';
import { Button, Label, Select, TextInput } from 'flowbite-react';

const UpdatePermission = () => {
  const { id } = useParams();

  const { onViewRole, onUpdatePermissions } = useRole();
  const { roleView } = useSelector((state) => state.role);

  const formik = useFormik({
    initialValues: {
      roleId: 1,
      permissions: [
        {
          permissionId: roleView?.permissions?.[0]?.['id'],
          status: roleView?.permissions?.[0]?.['status'],
        },
        {
          permissionId: roleView?.permissions?.[1]?.['id'],
          status: roleView?.permissions?.[1]?.['status'],
        },
        {
          permissionId: roleView?.permissions?.[2]?.['id'],
          status: roleView?.permissions?.[2]?.['status'],
        },
        {
          permissionId: roleView?.permissions?.[3]?.['id'],
          status: roleView?.permissions?.[3]?.['status'],
        },
        {
          permissionId: roleView?.permissions?.[4]?.['id'],
          status: roleView?.permissions?.[4]?.['status'],
        },
        {
          permissionId: roleView?.permissions?.[5]?.['id'],
          status: roleView?.permissions?.[5]?.['status'],
        },
        {
          permissionId: roleView?.permissions?.[6]?.['id'],
          status: roleView?.permissions?.[6]?.['status'],
        },
        {
          permissionId: roleView?.permissions?.[7]?.['id'],
          status: roleView?.permissions?.[7]?.['status'],
        },
      ],
    },
    enableReinitialize: true,
    onSubmit: (values) => {
      onUpdatePermissions(id, values);
    },
  });

  const permissions = formik.values.permissions;

  const permissionList = permissions.map((permission, index) => (
    <div key={permission.permissionId} className="grid gap-5 ">
      <div className='grid gap-2'>
        <div>
          <div className="mb-2 block">
            <Label value={`Permission ${index + 1}`} />
          </div>
          <Select
            name={`permissions[${index}].permissionId`}
            value={permission.permissionId}
            onChange={formik.handleChange}
          >
            {roleView?.permissions?.map((perm) => (
              <option key={perm.id} value={perm.id}>
                {perm.name}
              </option>
            ))}
          </Select>
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Status" />
          </div>
          <input
            className="inline-block"
            name={`permissions[${index}].status`}
            type="checkbox"
            checked={permission.status}
            onChange={() =>
              formik.setFieldValue(
                `permissions[${index}].status`,
                !permission.status
              )
            }
          />
        </div>
      </div>
    </div>
  ));

  useEffect(() => {
    onViewRole(id);
  }, [id]);

  return (
    <div className="p-4">
      <form onSubmit={formik.handleSubmit}>
        <div className="grid gap-5">
          <div>
            <div className="mb-2 block">
              <Label value="Role ID" />
            </div>
            <TextInput
              name="roleId"
              type="text"
              value={formik.values.roleId}
              onChange={formik.handleChange}
              required
            />
          </div>
        </div>

        {permissionList}

        <Button
          outline
          gradientDuoTone="purpleToBlue"
          type="submit"
          className="mt-4"
        >
          Submit
        </Button>
      </form>
    </div>
  );
};

export default UpdatePermission;
