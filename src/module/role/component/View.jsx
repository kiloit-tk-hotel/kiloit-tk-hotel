import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from 'react-router-dom'
import useRole from "../core/hook";


const View = () => {
  const { id } = useParams()
  const { onViewRole } = useRole();
  const { roleView } = useSelector((state) => state.role);
  console.log(roleView);

  useEffect(() => {
    onViewRole(id);
  }, [id]);

  return (
    <div >
      <div class="block max-w-sm p-8 bg-sky-300 border border-gray-400 rounded-lg shadow mx-5 my-3">
        <p className="py-3">Name: {roleView.role?.name}</p>
        <p className="py-3">ID: {roleView.role?.id}</p>
        <p className="py-3">Code: {roleView.role?.code}</p>
      </div>

      <div>
        {
          roleView.permissions?.map((permission) => {
            return (
              <>
                <ul key={permission.id}>
                  <li>{permission.name}</li>
                  <li>{permission.module}</li>
                  <li>{permission.status ? <>TRUE</> : <>FALSE</>} </li>
                </ul>

                <br />
              </>
            )
          })
        }
      </div>
    </div>
  );
};

export default View;
