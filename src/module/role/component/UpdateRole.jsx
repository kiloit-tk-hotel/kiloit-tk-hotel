import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Button, Label, TextInput } from "flowbite-react";
import useRole from '../core/hook'
import { useFormik } from 'formik';
import Swal from 'sweetalert2';

const UpdateRole = () => {
  const { id } = useParams()
  const { onViewRole, onUpdateRole } = useRole()
  const { roleView } = useSelector(state => state.role)
  console.log(roleView);
  const formik = useFormik({
    initialValues: {
      name: roleView?.role?.name,
      code: roleView?.role?.code,
    },
    enableReinitialize: true,
    onSubmit: values => {
      Swal.fire({
        position: "top-end",
        height: '100px',
        icon: "success",
        title: "Edit Successed",
        showConfirmButton: false,
        timer: 1500
      })
        .then(() => {
          onUpdateRole(id, values);
          formik.resetForm();
        })
        .catch((error) => {
          Swal.fire({
            title: "Error!",
            text: "Failed to update room type",
            icon: "error",
          });
        });

    }
  })

  useEffect(() => {
    onViewRole(id)
  }, [])

  return (
    <>
      <form onSubmit={formik.handleSubmit} className='w-[600px] mx-5'>
        <div className="grid gap-4  ">

          <div>
            <div className="mb-2 block">
              <Label value="name" />
            </div>
            <TextInput type="text" sizing="md" name='name' value={formik.values.name} onChange={formik.handleChange} />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="code" />
            </div>
            <TextInput type="text" sizing="md" name='code' value={formik.values.code} onChange={formik.handleChange} />
          </div>
        </div>
        <div className='mt-2 block '>
          <Button outline gradientDuoTone="purpleToBlue" type='submit'>Submit</Button>
        </div>
      </form>
    </>
  )
}

export default UpdateRole;