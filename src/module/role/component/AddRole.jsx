import { useFormik } from "formik";
import React from "react";
import useRole from "../core/hook";
import { Button, Label, TextInput } from "flowbite-react";
import Swal from "sweetalert2";

const AddRole = () => {
  const { onPostRole } = useRole();
  const formik = useFormik({
    initialValues: {
      name: "",
      code: "",
    },
    onSubmit: (values) => {
      Swal.fire({
        position: "top-end",
        height: "100px",
        icon: "success",
        title: "Add Successed",
        showConfirmButton: false,
        timer: 1500,
      })
        .then(() => {
          onPostRole(values);
        })
        .catch((error) => {
          Swal.fire({
            title: "Error!",
            text: "Failed to update room type",
            icon: "error",
          });
        });
    },
  });
  return (
    <form onSubmit={formik.handleSubmit} className="w-[600px] mx-5">
      <div className="grid gap-4  ">
        <div>
          <div className="mb-2 block">
            <Label value="name" />
          </div>
          <TextInput
            type="text"
            sizing="md"
            name="name"
            value={formik.values.name}
            onChange={formik.handleChange}
          />
        </div>

        <div>
          <div className="mb-2 block">
            <Label value="code" />
          </div>
          <TextInput
            type="text"
            sizing="md"
            name="code"
            value={formik.values.code}
            onChange={formik.handleChange}
          />
        </div>
      </div>
      <div className="mt-2 block ">
        <Button outline gradientDuoTone="purpleToBlue" type="submit">
          Submit
        </Button>
      </div>
    </form>
  );
};

export default AddRole;
