import { useEffect } from "react";
import useRole from "../../core/hook";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Badge, Button, Checkbox, Pagination, Table, TextInput } from "flowbite-react";
import Swal from "sweetalert2";
import Component from "../../../../components/spinner";
import { FiSearch } from "react-icons/fi";

const DisplayRoleUser = () => {
  const { onGetRole, onPaging, onHandlePage, onSearch } = useRole();
  const { onDeleteRole } = useRole()
  const { roleList, loading, paging, params } = useSelector((state) => state.role);
  const totalPage = paging.totalPage || 1;

  console.log(roleList);

  const { page, size } = params

  useEffect(() => {
    onGetRole();
    onPaging();
  }, [params, size]);

  const handleDelete = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        onDeleteRole(id);
        Swal.fire({
          title: "Deleted!",
          text: "Your file has been deleted.",
          icon: "success"
        });
      }
    });

  }


  return (
    <div className="shadow p-5">
      {loading ? (
        <>
          <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
            <Component />
          </div>
        </>
      ) : (
        <>
          <div className="flex justify-between items-center mb-5">
            <span className="text-md font-medium">Role</span>
            <Link to="/addrole">
              <Button outline gradientDuoTone="purpleToBlue">
                Add Role
              </Button>
            </Link>
          </div>

          <div className="my-5">
            <TextInput
              onChange={(e) => onSearch(e.target.value)}
              icon={FiSearch}
            />
          </div>

          <div className="overflow-x-auto shadow">
            <Table hoverable>
              <Table.Head>
                <Table.HeadCell>checkbox</Table.HeadCell>
                <Table.HeadCell>Name</Table.HeadCell>
                <Table.HeadCell>Id</Table.HeadCell>
                <Table.HeadCell>Code</Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">View</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Delete</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Update</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Update Permission</span>
                </Table.HeadCell>
              </Table.Head>
              <Table.Body className="divide-y">
                {roleList.map((role) => {
                  return (
                    <Table.Row
                      key={role.id}
                      className="bg-white dark:border-gray-700 dark:bg-gray-800"
                    >
                      <Table.Cell className="p-4">
                        <Checkbox />
                      </Table.Cell>
                      <Table.Cell>{role.name}</Table.Cell>
                      <Table.Cell>{role.id}</Table.Cell>
                      <Table.Cell>{role.code}</Table.Cell>
                      <Table.Cell>
                        <Link
                          to={`/viewRole/${role.id}`}
                          className="font-medium text-cyan-600 hover:underline dark:text-sky-500"
                        >
                          <Badge
                            color="success"
                            className="flex items-center justify-center"
                          >
                            View
                          </Badge>
                        </Link>
                      </Table.Cell>
                      <Table.Cell
                        onClick={() => handleDelete(role.id)}
                        className="font-medium text-red-600 hover:underline decor  cursor-pointer"
                      >
                        <Badge
                          color="failure"
                          className="flex items-center justify-center"
                        >
                          Delete
                        </Badge>
                      </Table.Cell>
                      <Table.Cell className="font-medium text-green-500-600 hover:underline decor cursor-pointer ">
                        <Link to={`/updateRole/${role.id}`}>
                          <Badge
                            color="purple"
                            size="md"
                            className="flex items-center justify-center"
                          >
                            Edit
                          </Badge>
                        </Link>
                      </Table.Cell>

                      <Table.Cell className="font-medium text-green-500-600 hover:underline decor cursor-pointer ">
                        <Link to={`/edit/permission/${role.id}`}>
                          <Badge
                            color="green"
                            size="md"
                            className="flex items-center justify-center"
                          >
                            Update Permission
                          </Badge>
                        </Link>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
              </Table.Body>
            </Table>
          </div>
          <div className="flex overflow-x-auto sm:justify-end">
            <Pagination
              currentPage={page}
              totalPages={totalPage}
              onPageChange={(page) => onHandlePage(page)}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default DisplayRoleUser;
