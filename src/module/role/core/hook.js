import { useDispatch, useSelector } from "react-redux";
import {
  setLoading,
  setPaging,
  setParams,
  setPermissionList,
  setRoleList,
  setRolePost,
  setRoleView,
} from "./roleSlice";
import {
  regRole,
  reqUserRole,
  reqViewRole,
  reqDeleteRole,
  reqUpdateRole,
  reqPaging,
  reqUpdatePermissions,
} from "./reguest";

const useRole = () => {
  const dispatch = useDispatch();
  const { params } = useSelector((state) => state.role);

  const onGetRole = () => {
    // dispatch(setLoading(true));
    regRole(params)
      .then((res) => {
        dispatch(setRoleList(res.data.data));
        // dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onPostRole = (payload) => {
    dispatch(setLoading(true));
    reqUserRole(payload)
      .then((res) => {
        console.log(res.data.data);
        dispatch(setRolePost(res.data.data));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onViewRole = (id) => {
    dispatch(setLoading(true));
    reqViewRole(id).then((res) => {
      dispatch(setRoleView(res.data.data));
      dispatch(setLoading(false));
    });
  };

  const onDeleteRole = (id) => {
    dispatch(setLoading(true));
    reqDeleteRole(id).then(() => {
      regRole();
      dispatch(setLoading(false));
    });
  };

  const onUpdateRole = (id, payload) => {
    dispatch(setLoading(true));
    reqUpdateRole(id, payload)
      .then((res) => {
        dispatch(setRoleView(res.data.data));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onUpdatePermissions = (id, payload) => {
    reqUpdatePermissions(id, payload)
      .then((res) => {
        dispatch(setPermissionList(res.data.data.permissions));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onPaging = () => {
    reqPaging(params)
      .then((res) => {
        dispatch(setPaging(res.data.paging));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onHandlePage = (page) => {
    dispatch(setParams({ ...params, page }));
  };

  const onSearch = (text) => {
    dispatch(
      setParams({
        query: text,
        page: 1,
      })
    );
  };

  return {
    onGetRole,
    onPostRole,
    onViewRole,
    onDeleteRole,
    onUpdateRole,
    onPaging,
    onHandlePage,
    onSearch,
    onUpdatePermissions,
  };
};

export default useRole;
