import { createSlice } from "@reduxjs/toolkit";

const roleSlice = createSlice({
  name: "role",
  initialState: {
    roleList: [],
    rolePost: [],
    roleView: {},
    permissionList: {},
    loading: false,
    totalPage: 0,
    paging: {},
    params: {
      query: "",
      code: "",
      order: "asc",
      page: 1,
      size: 12,
      sort: "id",
    },
  },
  reducers: {
    setRoleList: (state, action) => {
      state.roleList = action.payload;
    },
    setRolePost: (state, action) => {
      state.rolePost = action.payload;
    },
    setRoleView: (state, action) => {
      state.roleView = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setParams: (state, action) => {
      state.params = { ...state.params, ...action.payload };
    },
    setPaging: (state, action) => {
      state.paging = action.payload;
    },
    setPermissionList: (state, action) => {
      state.permissionList = action.payload;
    },
  },
});

export const {
  setRoleList,
  setRolePost,
  setRoleView,
  setLoading,
  setParams,
  setPaging,
  setPermissionList,
} = roleSlice.actions;

export default roleSlice.reducer;
