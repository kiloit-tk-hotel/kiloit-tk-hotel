import axios from "axios";

const regRole = (params) => {
  return axios.get("api/roles", {
    params: params,
  });
};

const reqUserRole = (payload = {}) => {
  return axios.post("api/roles", payload);
};

const reqViewRole = (id) => {
  return axios.get(`api/roles/${id}`);
};

const reqDeleteRole = (id) => {
  return axios.delete(`api/roles/${id}`);
};

const reqViewRoleById = (id) => {
  return axios.get(`api/roles/${id}`);
};

const reqUpdateRole = (id, payload = {}) => {
  return axios.put(`api/roles/${id}`, payload);
};

const reqUpdatePermissions = (id, payload = {}) => {
  return axios.put(`api/roles/${id}`, payload);
};

const reqPaging = (params) => {
  return axios.get("api/roles", {
    params: params,
  });
};

export {
  regRole,
  reqUserRole,
  reqViewRole,
  reqDeleteRole,
  reqViewRoleById,
  reqUpdateRole,
  reqPaging,
  reqUpdatePermissions,
};
