import { useFormik } from 'formik'
import React, { useEffect } from 'react'
import { Button, Label, TextInput } from "flowbite-react";
import { useSelector } from "react-redux";
import { useAuth } from '../../core/hook';

const ResetPassword = () => {

  const { onResetPassword } = useAuth()

  const formik = useFormik({
    initialValues: {
      oldPassword: "",
      password: "",
      confirmPassword: ""
    },
    onSubmit: (values) => {
      console.log(values);
      onResetPassword(values)
    }
  })


  return (
    <div className='absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-[400px]'>
      <div className='w-full  px-4 py-8 bg-white rounded-lg shadow-md overflow-hidden '>
        <form className="flex  flex-col gap-4" onSubmit={formik.handleSubmit}>
          <div className='grid gap-5 max-w-[700px] '>
            <div>
              <div className="mb-2 block">
                <Label value="Old Password" />
              </div>
              <TextInput type="text" name='oldPassword' value={formik.values.oldPassword} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="New Password" />
              </div>
              <TextInput type="text" name='password' value={formik.values.password} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="Confirm Password" />
              </div>
              <TextInput type="text" name='confirmPassword' value={formik.values.confirmPassword} onChange={formik.handleChange} required />
            </div>

            <Button type="submit">Reset Password</Button>

          </div>
        </form>
      </div>
    </div>
  )
}

export default ResetPassword