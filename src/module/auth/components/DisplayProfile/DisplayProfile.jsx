import React from 'react';
import { useSelector } from 'react-redux';
import { Avatar } from 'flowbite-react';
import { Link } from 'react-router-dom';
import { useAuth } from '../../core/hook';
import { FaCamera, FaTrash } from 'react-icons/fa'; // Import FontAwesome icons

const DisplayProfile = () => {
  const { profile } = useSelector((state) => state.auth);
  const { onUploadPic, onDeletePic } = useAuth();
  const fileInputRef = React.useRef(null);

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      onUploadPic(file);
      console.log(file);
    }
  };

  const handleIconClick = () => {
    fileInputRef.current.click();
  };

  return (
    <div className="overflow-x-auto overscroll-auto">
      <div className="rounded-lg bg-slate-50 mt-10 overflow-hidden shadow-md">
        <div className="bg-gradient-to-r from-violet-500 to-fuchsia-500 h-44"></div>
        <div className="relative">
          {profile && (
            <div className="relative">
              <Avatar
                img={profile.avatar}
                className="absolute left-16 -mt-7"
                status="online"
                rounded
                statusPosition="top-right"
                bordered
                color="success"
                size="lg"
              />
              <div className="absolute left-16 -mt-7 flex space-x-2">
                <button
                  type="button"
                  className="w-8 h-8 bg-gray-800 bg-opacity-75 rounded-full flex items-center justify-center text-white hover:bg-opacity-50"
                  onClick={handleIconClick}
                  aria-label="Upload Picture"
                >
                  <FaCamera size={18} />
                </button>

              </div>
              <input
                type="file"
                name="avatar"
                className="hidden"
                ref={fileInputRef}
                onChange={handleFileChange}
              />
            </div>
          )}
          <div className="flex items-center justify-between mt-2 ml-40">
            <div>
              <h1 className="text-2xl font-medium">{profile?.name}</h1>
              <span className="font-medium text-gray-500 text-sm">{profile?.email}</span>
            </div>
            <Link to="/profile/update">
              <button
                className="px-5 py-2 bg-white border border-blue-500 text-blue-500 rounded-lg mr-5 hidden md:block"
                aria-label="Edit Profile"
              >
                Edit Profile
              </button>
            </Link>
          </div>
        </div>
        <div className="flex gap-10 px-5 py-5 mt-10 border-t">
          <div className="text-blue-500">Overview</div>
          <div>Project</div>
          <div>Files</div>
          <div>Teams</div>
          <div>Followers</div>
          <div>Activity</div>
          <button
            type="button"
            className="w-7 h-7 bg-red-600 bg-opacity-75 rounded-full flex items-center justify-center text-white hover:bg-opacity-50"
            onClick={onDeletePic}
            aria-label="Delete Picture"
          >
            <FaTrash size={18} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default DisplayProfile;
