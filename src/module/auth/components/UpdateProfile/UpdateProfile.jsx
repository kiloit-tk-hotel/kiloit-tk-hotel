import React from 'react'
import { useSelector } from 'react-redux'
import { useAuth } from '../../core/hook'
import { useFormik } from 'formik'
import { Button, Label, TextInput } from "flowbite-react";

const UpdateProfile = () => {

  const { profile } = useSelector(state => state.auth)
  const { onUpdateProfile } = useAuth()

  const formik = useFormik({
    initialValues: {
      username: profile.username,
      name: profile.name,
      address: profile.address,
      bio: profile.bio,
      email: profile.email,
      phone: profile.phone,
    },
    onSubmit: values => {
      onUpdateProfile(values)
    }
  })



  return (
    <div className='absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-[700px]'>
      <div className='w-full  px-4 py-8 bg-white rounded-lg shadow-md overflow-hidden '>
        <form className="flex  flex-col gap-4" onSubmit={formik.handleSubmit}>
          <div className='grid md:grid-cols-2 gap-5'>
            <div>
              <div className="mb-2 block">
                <Label value="username" />
              </div>
              <TextInput type="text" name='username' value={formik.values.username} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="name" />
              </div>
              <TextInput type="text" name='name' value={formik.values.name} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="address" />
              </div>
              <TextInput type="text" name='address' value={formik.values.address} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="email" />
              </div>
              <TextInput type="text" name='email' value={formik.values.email} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="phone" />
              </div>
              <TextInput type="text" name='phone' value={formik.values.phone} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="bio" />
              </div>
              <TextInput type="text" name='bio' value={formik.values.bio} onChange={formik.handleChange} required />
            </div>
          </div>
          <div>
            <Button type="submit">Reset Password</Button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default UpdateProfile