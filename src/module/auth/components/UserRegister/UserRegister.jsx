import { useFormik } from 'formik'
import React from 'react'
import { useAuth } from '../../core/hook'
import { Button, Checkbox, Label, TextInput } from "flowbite-react";
import register from '../../../../assets/image/register.svg'

const UserRegister = () => {

  const { onRegister } = useAuth()

  const formik = useFormik({
    initialValues: {
      username: "",
      name: "",
      phone: "",
      email: "",
      password: "",
      address: "",
      status: "",
      bio: ""
    },
    onSubmit: (values) => {
      onRegister(values)
      formik.resetForm()
    }
  })
  return (
    <div className='absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2'>
      <h1 className='font-medium text-xl mb-5'>Register</h1>
      <div className='w-full h-full grid md:grid-cols-2 gap-4 px-4 py-8 bg-white rounded-lg shadow-md overflow-hidden'>
        <div>
          <img src={register} alt="" />
        </div>
        <form className="flex flex-col gap-4" onSubmit={formik.handleSubmit}>
          <div className='grid grid-cols-2 gap-5'>
            <div>
              <div className="mb-2 block">
                <Label value="Username" />
              </div>
              <TextInput type="text" name='username' value={formik.values.username} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="Name" />
              </div>
              <TextInput type="text" name='name' value={formik.values.name} onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="phone" />
              </div>
              <TextInput type="text" name='phone' value={formik.values.phone} onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="email" />
              </div>
              <TextInput type="text" name='email' value={formik.values.email} onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="password" />
              </div>
              <TextInput type="text" name='password' value={formik.values.password} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="address" />
              </div>
              <TextInput type="text" name='address' value={formik.values.address} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="status" />
              </div>
              <TextInput type="text" name='status' value={formik.values.status} onChange={formik.handleChange} required />
            </div>

            <div>
              <div className="mb-2 block">
                <Label value="bio" />
              </div>
              <TextInput type="text" name='bio' value={formik.values.bio} onChange={formik.handleChange} required />
            </div>

            <div className="flex items-center gap-2">
              <Checkbox id="remember" />
              <Label htmlFor="remember">Remember me</Label>
            </div>

            <Button type="submit">Register</Button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default UserRegister