import axios from "axios";

const regLogin = (payload = {}) => {
  return axios.post("api/auth/login", payload, {
    headers: {
      Authorization: `Basic aG90ZWw6aG90ZWxAMTIz`,
    },
  });
};

const regProfile = () => {
  return axios.get("api/users/getProfile");
};

const regUpdateProfile = (payload = {}) => {
  return axios.put("api/users/updateProfile", payload);
};

const regRegister = (payload = {}) => {
  return axios.post("api/auth/register", payload, {
    headers: {
      Authorization: `Basic aG90ZWw6aG90ZWxAMTIz`,
    },
  });
};

const regResetPassword = (payload = {}) => {
  return axios.put("api/users", payload);
};

const reqUploadPic = (payload) => {
  return axios.post("api/users/image", payload);
};

const reqDeletePic = () => {
  return axios.delete("api/users/image");
};

export {
  regLogin,
  regProfile,
  regUpdateProfile,
  regRegister,
  regResetPassword,
  reqUploadPic,
  reqDeletePic,
};
