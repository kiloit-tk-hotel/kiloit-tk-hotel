import { useDispatch } from "react-redux";
import {
  regLogin,
  regProfile,
  regRegister,
  regResetPassword,
  regUpdateProfile,
  reqUploadPic,
  reqDeletePic, // Assuming reqDeletePic is defined in ./request
} from "./reguest";
import { setProfile, setToken } from "./authSlice";

const useAuth = () => {
  const dispatch = useDispatch();

  const onLogin = (payload) => {
    regLogin(payload)
      .then((res) => {
        dispatch(setToken(res.data.data.token));
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const onGetProfile = () => {
    regProfile()
      .then((res) => {
        dispatch(setProfile(res.data.data));
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const onUpdateProfile = (payload) => {
    regUpdateProfile(payload)
      .then(() => {
        onGetProfile();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const onRegister = (payload) => {
    regRegister(payload)
      .then((res) => {
        dispatch(setProfile(res.data.data.avatar));
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const onResetPassword = (payload) => {
    regResetPassword(payload)
      .then((res) => {
        console.log(res.data.data);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const onUploadPic = (file) => {
    const formData = new FormData();
    formData.append("image", file);

    reqUploadPic(formData)
      .then(() => {
        onGetProfile();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const onDeletePic = () => {
    reqDeletePic()
      .then(() => {
        onGetProfile();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  return {
    onLogin,
    onGetProfile,
    onUpdateProfile,
    onRegister,
    onResetPassword,
    onUploadPic,
    onDeletePic,
  };
};

export { useAuth };
