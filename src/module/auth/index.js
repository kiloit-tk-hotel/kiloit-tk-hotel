import React from "react";
import { useFormik } from "formik";
import login from "../../assets/image/login.jpg";
import { Link } from "react-router-dom";
import { useAuth } from "./core/hook";

const LoginForm = () => {
  const { onLogin } = useAuth();

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    onSubmit: (values) => {
      onLogin(values);
    },
  });
  return (
    <div className="grid md:grid-cols-2 gap-12 max-w-[1200px] h-[100vh] mx-auto justify-center items-center p-5">
      <div className="w-full">
        <img className="w-full" src={login} alt="" />
      </div>

      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-col gap-5 w-full"
      >
        <div className="flex flex-col gap-2">
          <label>USER NAME</label>
          <input
            type="text"
            name="username"
            className="w-full"
            onChange={formik.handleChange}
            value={formik.values.username}
          />
        </div>
        <div className="flex flex-col gap-2">
          <label>PASSWORD</label>
          <input
            type="text"
            name="password"
            className="w-full"
            onChange={formik.handleChange}
            value={formik.values.password}
          />
        </div>

        <button
          type="submit"
          className="px-9 py-3 bg-teal-500 rounded-sm uppercase font-bold text-white"
        >
          Login
        </button>

        <div className="flex gap-2">
          <Link to="/signup">
            <span className="text-xs font-medium text-gray-500 hover:underline">
              Register /
            </span>
          </Link>

          <Link to="/resetPassword">
            <span className="text-xs font-medium text-gray-500 hover:underline">
              Forgot Password?
            </span>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default LoginForm;
