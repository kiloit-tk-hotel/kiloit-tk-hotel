import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import useRoomType from '../../core/hook'
import { useSelector } from 'react-redux'
import { useFormik } from 'formik'
import { Button, Label, TextInput } from "flowbite-react";
import Swal from 'sweetalert2'
import Component from '../../../../components/spinner'

const EditRoomType = () => {
  const { id } = useParams()
  const { onViewById, onUpdateRoomType } = useRoomType()
  const { viewRoom, loading } = useSelector(state => state.roomType)

  const formik = useFormik({
    initialValues: {
      title: viewRoom.title,
      sub_title: viewRoom.sub_title,
      description: viewRoom.description,
      bed: viewRoom.bed,
      adult: viewRoom.adult,
      children: viewRoom.children,
      price: viewRoom.price,
      amenity: viewRoom.amenity,
    },
    enableReinitialize: true,
    onSubmit: (values) => {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.onmouseenter = Swal.stopTimer;
          toast.onmouseleave = Swal.resumeTimer;
        }
      });
      Toast.fire({
        icon: "success",
        title: "Edit successfully"
      });

      onUpdateRoomType(id, values);
    },

  })

  useEffect(() => {
    onViewById(id)
  }, [])

  return (
    <>
      <div className='w-full mx-auto rounded-lg shadow bg-white p-8   mt-5'>
        <h1 className='text-xl mb-4'>Edit Room Types</h1>
        {
          loading ? <>
            <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
              <Component />
            </div>
          </> : <>
            <form onSubmit={formik.handleSubmit}>
              <div className="grid md:grid-cols-2 gap-4">

                <div>
                  <div className="mb-2 block">
                    <Label value="title" />
                  </div>
                  <TextInput type="text" sizing="md" name='title' value={formik.values.title} onChange={formik.handleChange} />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label value="sub_title" />
                  </div>
                  <TextInput type="text" sizing="md" name='sub_title' value={formik.values.sub_title} onChange={formik.handleChange} />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label value="description" />
                  </div>
                  <TextInput type="text" sizing="md" name='description' value={formik.values.description} onChange={formik.handleChange} />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label value="bed" />
                  </div>
                  <TextInput type="text" sizing="md" name='bed' value={formik.values.bed} onChange={formik.handleChange} />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label value="adult" />
                  </div>
                  <TextInput type="text" sizing="md" name='adult' value={formik.values.adult} onChange={formik.handleChange} />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label value="children" />
                  </div>
                  <TextInput type="text" sizing="md" name='children' value={formik.values.children} onChange={formik.handleChange} />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label value="price" />
                  </div>
                  <TextInput type="text" sizing="md" name='price' value={formik.values.price} onChange={formik.handleChange} />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label value="amenity" />
                  </div>
                  <TextInput type="text" sizing="md" name='amenity' value={formik.values.amenity} onChange={formik.handleChange} />
                </div>

              </div>
              <div className='mt-2 block'>
                <Button type='submit'>Edit</Button>
              </div>
            </form>
          </>
        }

      </div>
    </>
  )
}

export default EditRoomType