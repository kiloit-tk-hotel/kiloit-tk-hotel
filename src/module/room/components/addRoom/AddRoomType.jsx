import { useFormik } from "formik";
import React from "react";
import useRoomType from "../../core/hook";
import { Button, Checkbox, Label, TextInput } from "flowbite-react";

const AddRoomType = () => {
  const { onGetAddRoomType } = useRoomType();

  const formik = useFormik({
    initialValues: {
      title: "",
      sub_title: "",
      description: "",
      bed: "",
      adult: "",
      children: "",
      price: "",
      amenity: "",
    },
    onSubmit: (values) => {
      onGetAddRoomType(values);
      formik.resetForm();
    },

  });
  return (
    <div className="p-4">
      <form onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-2 gap-5">
          <div>
            <div className="mb-2 block">
              <Label value="Title" />
            </div>
            <TextInput name="title" type="text" value={formik.values.title}
              onChange={formik.handleChange} required />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Sub-Title" />
            </div>
            <TextInput name="sub_title" type="text" value={formik.values.sub_title}
              onChange={formik.handleChange} required />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Description" />
            </div>
            <TextInput name="description" type="text" value={formik.values.description}
              onChange={formik.handleChange} required />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Bed" />
            </div>
            <TextInput name="bed" type="text" value={formik.values.bed}
              onChange={formik.handleChange} required />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Adult" />
            </div>
            <TextInput name="adult" type="text" value={formik.values.adult}
              onChange={formik.handleChange} required />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Children" />
            </div>
            <TextInput name="children" type="text" value={formik.values.children}
              onChange={formik.handleChange} required />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Price" />
            </div>
            <TextInput name="price" type="text" value={formik.values.price}
              onChange={formik.handleChange} required />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Amenity" />
            </div>
            <TextInput name="amenity" type="text" value={formik.values.amenity}
              onChange={formik.handleChange} required />
          </div>
        </div>

        <Button outline gradientDuoTone="purpleToBlue" type="submit" className=" mt-4">Submit</Button>
      </form>

    </div >
  );
};

export default AddRoomType;
