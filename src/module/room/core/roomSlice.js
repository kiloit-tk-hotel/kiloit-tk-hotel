import { createSlice } from "@reduxjs/toolkit";

const roomSlice = createSlice({
  name: "roomType",
  initialState: {
    roomTypes: [],
    roomType: [],
    viewRoom: [],
    loading: [],
    totalPage: 0,
    paging: {},
    params: {
      query: "",
      page: 1,
      size: 12,
      order: "asc",
      sort: "id",
    },
  },
  reducers: {
    setRoomTypes: (state, action) => {
      state.roomTypes = action.payload;
    },
    setRoomType: (state, action) => {
      state.roomType = action.payload;
    },
    setViewRoom: (state, action) => {
      state.viewRoom = action.payload;
    },
    setDeleteRoom: (state, action) => {
      state.roomTypes = state.roomTypes.filter(
        (room) => room.id !== action.payload
      );
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setParams: (state, action) => {
      state.params = { ...state.params, ...action.payload };
    },
    setPaging: (state, action) => {
      state.paging = action.payload;
    },
  },
});

export const {
  setRoomTypes,
  setRoomType,
  setViewRoom,
  setDeleteRoom,
  setLoading,
  setSortConfig,
  setParams,
  setPaging,
} = roomSlice.actions;

export default roomSlice.reducer;
