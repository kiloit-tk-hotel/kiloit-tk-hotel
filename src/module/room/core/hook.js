import { useDispatch, useSelector } from "react-redux";
import {
  regAddRoomType,
  regDeleteRoomType,
  regRoomType,
  regUpdateRoomType,
  regViewRoomById,
  reqPaging,
} from "./reguest";
import {
  setDeleteRoom,
  setLoading,
  setPaging,
  setParams,
  setRoomType,
  setRoomTypes,
  setViewRoom,
} from "./roomSlice";
import Swal from "sweetalert2";

const useRoomType = () => {
  const dispatch = useDispatch();
  const { params } = useSelector((state) => state.roomType);
  const { sort, order } = params;
  const onGetRoomType = () => {
    dispatch(setLoading(true));
    regRoomType(params).then((res) => {
      dispatch(setRoomTypes(res.data.data));
    });
    dispatch(setLoading(false));
  };

  const onGetAddRoomType = (payload = {}) => {
    dispatch(setLoading(true));
    regAddRoomType(payload)
      .then((res) => {
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: "success",
          title: "Post successfully",
        });
        dispatch(setRoomType(res.data.data));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err);
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: "error",
          title: "post fail",
        });
      });
  };

  const onDeleteRoomType = (id) => {
    dispatch(setLoading(true));
    regDeleteRoomType(id).then(() => {
      dispatch(setDeleteRoom(id));
      dispatch(setLoading(false));
    });
  };

  const onViewById = (id) => {
    dispatch(setLoading(true));
    regViewRoomById(id).then((res) => {
      dispatch(setViewRoom(res.data.data));
      dispatch(setLoading(false));
    });
  };

  const onUpdateRoomType = (id, payload) => {
    dispatch(setLoading(true));
    regUpdateRoomType(id, payload)
      .then((res) => {
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: "success",
          title: "Edit is successfully",
        });
        dispatch(setRoomType(res.data.data));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: "error",
          title: "Edit fail",
        });
        console.log(err);
        dispatch(setLoading(false));
      });
  };

  const onSearch = (text) => {
    dispatch(
      setParams({
        query: text,
        page: 1,
      })
    );
  };

  const onHandlePage = (page) => {
    dispatch(
      setParams({
        page: page,
      })
    );
  };

  const handleSort = (field) => {
    const newOrder = sort === field && order === "asc" ? "desc" : "asc";
    dispatch(setParams({ sort: field, order: newOrder }));
  };

  const onPaging = () => {
    reqPaging(params).then((res) => {
      dispatch(setPaging(res.data.paging));
    });
  };

  return {
    onGetRoomType,
    onDeleteRoomType,
    onGetAddRoomType,
    onViewById,
    onUpdateRoomType,
    onSearch,
    onHandlePage,
    onPaging,
    handleSort,
  };
};

export default useRoomType;
