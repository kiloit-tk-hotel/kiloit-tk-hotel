import axios from "axios";

const regRoomType = (params) => {
  return axios.get("api/v1.0.0/room-type", {
    params: params,
  });
};

const regAddRoomType = (payload = {}) => {
  return axios.post("api/v1.0.0/room-type", payload);
};

const regViewRoomById = (id) => {
  return axios.get(`api/v1.0.0/room-type/${id}`);
};

const regDeleteRoomType = (id) => {
  return axios.delete(`api/v1.0.0/room-type/${id}`);
};

const regUpdateRoomType = (id, payload = {}) => {
  return axios.put(`api/v1.0.0/room-type/${id}`, payload);
};

const reqPaging = (params) => {
  return axios.get("api/v1.0.0/room-type", {
    params: params,
  });
};

export {
  regRoomType,
  regDeleteRoomType,
  regAddRoomType,
  regViewRoomById,
  regUpdateRoomType,
  reqPaging,
};
