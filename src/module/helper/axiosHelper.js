import axios from "axios";

export const setUpAxios = () => {
  axios.defaults.baseURL = "http://13.214.207.172:8002/";
  axios.interceptors.request.use((config) => {
    const token = localStorage.getItem("token");
    if (token)
      config.headers.Authorization = "Bearer " + localStorage.getItem("token");
    return config;
  });
  axios.interceptors.response.use(
    function (response) {
      return response;
    },
    function (error) {
      if (error.response.status === 401) {
        localStorage.removeItem("token");
        window.location.reload();
      }
      return Promise.reject(error);
    }
  );
};
