import React, { useEffect } from "react";

import { useSelector } from "react-redux";
import {
  Badge,
  Button,
  Checkbox,
  Pagination,
  Table,
  TextInput,
} from "flowbite-react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { useRoomManagement } from "./core/hook";
import Component from "../../components/spinner";
import { FiSearch } from "react-icons/fi";
import { TiArrowSortedDown } from "react-icons/ti";

const DisplayRoomManagement = () => {
  const {
    onGetRoomManagement,
    onDeleteRoomManagement,
    onSearch,
    onHandlePage,
  } = useRoomManagement();
  const { roomManagementList, loading, params } = useSelector(
    (state) => state.roomManagement
  );

  const totalPage = roomManagementList.length;
  const { page } = params;

  const handleDelete = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        onDeleteRoomManagement(id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  };

  useEffect(() => {
    onGetRoomManagement();
  }, [params, page]);

  return (
    <div className=" shadow p-5">
      {loading ? (
        <>
          <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
            <Component />
          </div>
        </>
      ) : (
        <>
          <div className="mb-5 flex justify-between items-center">
            <span className="text-md font-medium">Room Type</span>
            <Link to="/addRoomManagement">
              <Button outline gradientDuoTone="purpleToBlue">
                Create Room
              </Button>
            </Link>
          </div>

          <div className="my-5">
            <TextInput
              icon={FiSearch}
              onChange={(e) => onSearch(e.target.value)}
            />
          </div>
          <div className="overflow-x-auto shadow">
            <Table hoverable>
              <Table.Head>
                <Table.HeadCell>Checkbox</Table.HeadCell>
                <Table.HeadCell>ID</Table.HeadCell>
                <Table.HeadCell>Status</Table.HeadCell>
                <Table.HeadCell>Floor</Table.HeadCell>
                <Table.HeadCell className="flex gap-2 items-center">
                  <p>Title of roomType</p>
                  <TiArrowSortedDown size={20} />
                </Table.HeadCell>
                <Table.HeadCell>roomType Id</Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">View</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Delete</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Update</span>
                </Table.HeadCell>
              </Table.Head>
              <Table.Body className="divide-y">
                {roomManagementList.map((room) => {
                  return (
                    <Table.Row
                      key={room.id}
                      className="bg-white dark:border-gray-700 dark:bg-gray-800"
                    >
                      <Table.Cell className="p-4">
                        <Checkbox />
                      </Table.Cell>
                      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                        {room.id}
                      </Table.Cell>
                      <Table.Cell>{room.status}</Table.Cell>
                      <Table.Cell>{room.floor}</Table.Cell>
                      <Table.Cell>{room.roomType.title}</Table.Cell>
                      <Table.Cell>{room.roomType.id}</Table.Cell>
                      <Table.Cell>
                        <Link
                          to={`/viewRoomManagement/${room.id}`}
                          className="font-medium text-cyan-600 hover:underline dark:text-sky-500"
                        >
                          <Badge
                            color="success"
                            className="flex items-center justify-center p-0"
                          >
                            View
                          </Badge>
                        </Link>
                      </Table.Cell>
                      <Table.Cell
                        onClick={() => handleDelete(room.id)}
                        className="font-medium text-red-600 hover:underline decor cursor-pointer "
                      >
                        <Badge
                          color="failure"
                          className="flex items-center justify-center p-0"
                        >
                          Delete
                        </Badge>
                      </Table.Cell>
                      <Table.Cell className="font-medium text-green-500-600 hover:underline decor cursor-pointer">
                        <Link to={`/updateRoomManagement/${room.id}`}>
                          <Badge
                            color="purple"
                            size="md"
                            className="flex items-center justify-center p-0"
                          >
                            Edit
                          </Badge>
                        </Link>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
              </Table.Body>
            </Table>
          </div>
          <div className="flex overflow-x-auto sm:justify-end">
            <Pagination
              totalPages={page}
              currentPage={totalPage}
              onPageChange={(page) => onHandlePage(page)}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default DisplayRoomManagement;
