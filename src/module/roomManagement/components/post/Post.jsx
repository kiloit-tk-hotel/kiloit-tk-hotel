import { useFormik } from 'formik'
import React, { useEffect } from 'react'
import { Button, Label, Select, TextInput } from 'flowbite-react';
import { useRoomManagement } from '../../core/hook';
import { useSelector } from 'react-redux';
import useRoomType from '../../../room/core/hook';


const Post = () => {
  const { onGetRoomType } = useRoomType()
  const { onPostRoomManagement, onGetRoomManagement } = useRoomManagement();
  const { roomTypes } = useSelector(state => state.roomType)

  const formik = useFormik({
    initialValues: {
      room: "",
      floor: "",
      status: "AVAILABLE",
      room_type_id: ""
    },
    onSubmit: (values) => {
      console.log(values)
      onPostRoomManagement(values)
    }
  })

  useEffect(() => {
    onGetRoomManagement();
    onGetRoomType();
  }, [])

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <div className="grid md:grid-cols-2 gap-4">

          <div>
            <div className="mb-2 block">
              <Label value="room" />
            </div>
            <TextInput type="text" sizing="md" name='room' value={formik.values.room} onChange={formik.handleChange} />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="floor" />
            </div>
            <TextInput type="text" sizing="md" name='floor' value={formik.values.floor} onChange={formik.handleChange} />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Status" />
            </div>
            <TextInput type="text" sizing="md" name='status' value={formik.values.status} onChange={formik.handleChange} />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="room_type" />
            </div>
            <Select name='room_type_id' onChange={formik.handleChange} id="countries" required>
              {
                roomTypes.map((roomType) => (
                  <option key={roomType.id} value={roomType.id}>{roomType.title}</option>
                ))
              }
            </Select>
          </div>
        </div>
        <div className='mt-2 block'>
          <Button type='submit'>Add</Button>
        </div>
      </form>
    </div>
  )
}

export default Post