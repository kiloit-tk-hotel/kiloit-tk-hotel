import { useFormik } from 'formik';
import React, { useEffect } from 'react'
import { useRoomManagement } from '../../core/hook';
import { useSelector } from 'react-redux';
import { Button, Label, Select, TextInput } from 'flowbite-react';
import { useParams } from 'react-router-dom';
import Component from '../../../../components/spinner';
import useRoomType from '../../../room/core/hook';

const EditRoomManagement = () => {
  const { id } = useParams()
  const { onEditRoomManagement, onViewById, onGetRoomManagement } = useRoomManagement()
  const { roomView, loading } = useSelector((state) => state.roomManagement)
  const { roomTypes } = useSelector((state) => state.roomType)
  const { onGetRoomType } = useRoomType()


  const formik = useFormik({
    initialValues: {
      room: roomView.room,
      floor: roomView.floor,
      status: roomView.status,
      room_type_id: roomView.room_type_id
    },
    enableReinitialize: true,
    onSubmit: (values) => {
      onEditRoomManagement(id, values)
      formik.resetForm()
    }
  })

  useEffect(() => {
    onViewById(id)
    onGetRoomType()
  }, [id])

  useEffect(() => {
    onGetRoomManagement()
  }, [])


  return (
    <div className='w-full shadow p-8 bg-white rounded-lg'>
      <h1 className='text-xl mb-4'>Edit Room Management</h1>
      {
        loading ? <>
          <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
            <Component />
          </div>
        </> : <>
          <form onSubmit={formik.handleSubmit}>
            <div className="grid md:grid-cols-2 gap-4">

              <div>
                <div className="mb-2 block">
                  <Label value="room" />
                </div>
                <TextInput type="text" sizing="md" name='room' value={formik.values.room} onChange={formik.handleChange} />
              </div>

              <div>
                <div className="mb-2 block">
                  <Label value="floor" />
                </div>
                <TextInput type="text" sizing="md" name='floor' value={formik.values.floor} onChange={formik.handleChange} />
              </div>

              <div>
                <div className="mb-2 block">
                  <Label value="Status" />
                </div>
                <Select name='status' onChange={formik.handleChange} id="countries" required>
                  <option value="AVAILABLE">Availble</option>
                  <option value="OCCUPIED">Occupied</option>
                  <option value="RESERVED">Reserved</option>
                  <option value="MAINTENANCE">Maintenance</option>
                </Select>
              </div>

              <div>
                <div className="mb-2 block">
                  <Label value="room_type" />
                </div>
                <Select name='room_type_id' onChange={formik.handleChange} id="countries" required>
                  {roomTypes.map((roomType) => (
                    <option key={roomType.id} value={roomType.id}>{roomType.title}</option>
                  ))}
                </Select>
              </div>
            </div>
            <div className='mt-2 block'>
              <Button type='submit'>Edit</Button>
            </div>
          </form>
        </>
      }
    </div>
  )
}

export default EditRoomManagement