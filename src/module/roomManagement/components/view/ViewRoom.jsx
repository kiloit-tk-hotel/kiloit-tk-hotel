import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useRoomManagement } from '../../core/hook'
import { useSelector } from 'react-redux'
import Component from '../../../../components/spinner'
import hotel from '../../../../assets/image/hotel.jpg'

const ViewRoom = () => {
  const { id } = useParams()
  const { onViewById } = useRoomManagement()
  const { roomView, loading } = useSelector(state => state.roomManagement)

  useEffect(() => {
    onViewById(id)
  }, [id])
  return (
    <div>
      {
        loading ?
          <>
            <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
              <Component />
            </div>
          </> : <>
            <div className='flex gap-5'>
              <div className='w-72 h-72'>
                <img src={hotel} alt="" />
              </div>
              <div>
                <p>ID: {roomView.id}</p>
                <p>status: {roomView.status}</p>
                <p>room: {roomView.room}</p>
                <p>floor: {roomView.floor}</p>
              </div>
            </div>

          </>
      }

    </div>
  )
}

export default ViewRoom