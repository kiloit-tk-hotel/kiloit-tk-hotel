import { useDispatch, useSelector } from "react-redux";
import {
  regDeleteRoomManagement,
  regEditRoomManagement,
  regPostRoomManagement,
  regRoomManagement,
  regViewRoomById,
} from "./reguest";
import {
  setDeletRoomManagement,
  setLoading,
  setPaging,
  setParams,
  setPostRoomManagement,
  setRoomManagement,
  setRoomView,
} from "./roomManagementSlice";
import Swal from "sweetalert2";

const useRoomManagement = () => {
  const dispatch = useDispatch();
  const { params } = useSelector((state) => state.roomManagement);
  const { sort, order } = params;

  const onGetRoomManagement = async () => {
    try {
      const res = await regRoomManagement(params);
      dispatch(setRoomManagement(res.data.data));
      dispatch(setLoading(true));
    } catch (err) {
      console.log(err);
    } finally {
      dispatch(setLoading(false));
    }
  };

  const onPostRoomManagement = (payload) => {
    dispatch(setLoading(true));
    return regPostRoomManagement(payload)
      .then((res) => {
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: "success",
          title: "Post successfully",
        });
        dispatch(setPostRoomManagement(res.data.data));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err);
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: "error",
          title: "Post fail",
        });
        dispatch(setLoading(false));
      });
  };

  const onViewById = (id) => {
    dispatch(setLoading(true));
    regViewRoomById(id).then((res) => {
      dispatch(setRoomView(res.data.data));
      dispatch(setLoading(false));
    });
  };

  const onDeleteRoomManagement = (id) => {
    dispatch(setLoading(true));
    regDeleteRoomManagement(id)
      .then(() => {
        dispatch(setDeletRoomManagement(id));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onEditRoomManagement = (id, payload) => {
    dispatch(setLoading(true));
    return regEditRoomManagement(id, payload)
      .then((res) => {
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: "success",
          title: "Edit successfully",
        });
        dispatch(setPostRoomManagement(res.data.data));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err);
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: "error",
          title: "Edit fail",
        });
        dispatch(setLoading(false));
      });
  };

  const onSearch = (text) => {
    dispatch(
      setParams({
        query: text,
        page: 1,
      })
    );
  };

  const onHandlePage = (page) => {
    dispatch(
      setParams({
        page: page,
      })
    );
  };

  const handleSort = (field) => {
    const newOrder = sort === field && order === "asc" ? "desc" : "asc";
    dispatch(setParams({ sort: field, order: newOrder }));
  };

  const onPaging = () => {
    regRoomManagement(params).then((res) => {
      dispatch(setPaging(res.data.paging));
    });
  };

  return {
    onGetRoomManagement,
    onPostRoomManagement,
    onDeleteRoomManagement,
    onViewById,
    onEditRoomManagement,
    onSearch,
    onHandlePage,
    onPaging,
    handleSort,
  };
};

export { useRoomManagement };
