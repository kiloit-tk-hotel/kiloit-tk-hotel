import axios from "axios";

const regRoomManagement = (params) => {
  return axios.get("api/v1.0.0/room", {
    params: params,
  });
};

const regPostRoomManagement = (payload = {}) => {
  return axios.post("api/v1.0.0/room", payload);
};

const regDeleteRoomManagement = (id) => {
  return axios.delete(`api/v1.0.0/room/${id}`);
};

const regViewRoomById = (id) => {
  return axios.get(`api/v1.0.0/room/${id}`);
};

const regEditRoomManagement = (id, payload = {}) => {
  return axios.put(`api/v1.0.0/room/${id}`, payload);
};

export {
  regRoomManagement,
  regPostRoomManagement,
  regDeleteRoomManagement,
  regViewRoomById,
  regEditRoomManagement,
};
