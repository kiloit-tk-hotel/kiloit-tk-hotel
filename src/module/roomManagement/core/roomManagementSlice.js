import { createSlice } from "@reduxjs/toolkit";

const roomManagementSlice = createSlice({
  name: "roomManagement",
  initialState: {
    roomManagementList: [],
    roomManagement: [],
    roomView: [],
    loading: false,
    totalPage: 0,
    paging: {},
    params: {
      query: "",
      page: 1,
      size: 12,
      order: "asc",
      sort: "id",
    },
  },
  reducers: {
    setRoomManagement: (state, action) => {
      state.roomManagementList = action.payload;
    },
    setPostRoomManagement: (state, action) => {
      state.roomManagement = action.payload;
    },
    setDeletRoomManagement: (state, action) => {
      state.roomManagementList = state.roomManagementList.filter(
        (room) => room.id !== action.payload
      );
    },
    setRoomView: (state, action) => {
      state.roomView = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setParams: (state, action) => {
      state.params = { ...state.params, ...action.payload };
    },
    setPaging: (state, action) => {
      state.paging = action.payload;
    },
  },
});

export const {
  setRoomManagement,
  setPostRoomManagement,
  setDeletRoomManagement,
  setRoomView,
  setLoading,
  setParams,
  setPaging,
} = roomManagementSlice.actions;

export default roomManagementSlice.reducer;
