import { defaults } from "chart.js/auto";
import { Doughnut, Bar, Line } from "react-chartjs-2";
import { useSelector } from 'react-redux'
import useRoomType from "../room/core/hook";
import { useEffect } from "react";
import { useRoomManagement } from "../roomManagement/core/hook";
defaults.maintainAspectRatio = false;
defaults.responsive = true;

const Home = () => {
  const { onGetRoomType } = useRoomType()
  const { onGetRoomManagement } = useRoomManagement()
  const { roomTypes } = useSelector(state => state.roomType)
  const { roomManagementList } = useSelector(state => state.roomManagement)


  useEffect(() => {
    onGetRoomType()
    onGetRoomManagement()
  }, [])


  return (
    <div className="p-10">

      <div className="bg-gradient-to-r from-sky-500 to-indigo-500 h-40 p-3 mb-5">
        <h2 className="w-full h-full flex items-center justify-center font-medium text-2xl text-gray-300">Welcome To My Admin Dashboard</h2>
      </div>

      <div className=" grid gap-5 ">

        <div className="shadow w-full h-[500px]">
          <Line
            data={{
              labels: roomTypes.map((rooms) => rooms.title),
              datasets: [
                {
                  label: 'Price',
                  data: roomTypes.map((room) => room.price),
                  backgroundColor: '#FFB1C1',
                  borderRadius: 5
                },
                {
                  label: 'Adult',
                  data: roomTypes.map((room) => room.adult),
                  backgroundColor: '#9BD0F5'
                }
              ]
            }}
          />
        </div>

        <div className="grid md:grid-cols-2 gap-3">

          <div className="shadow w-full h-[500px]">
            <Bar
              data={{
                labels: roomTypes.map((room) => room.title),
                datasets: [
                  {
                    label: 'Price',
                    data: roomTypes.map((room) => room.price),
                    backgroundColor: '#FFB1C1',
                    borderRadius: 5
                  },
                  {
                    label: 'Bed',
                    data: roomTypes.map((room) => room.bed),
                    backgroundColor: '#9BD0F5'
                  },
                  {
                    label: 'Children',
                    data: roomTypes.map((room) => room.children),
                    backgroundColor: 'red'
                  }
                ]
              }}
            />
          </div>

          <div className="shadow w-full h-[500px]">
            <Doughnut
              data={{
                labels: roomManagementList.map((rm) => rm.roomType.title),
                datasets: [
                  {
                    label: 'Floor',
                    data: roomManagementList.map((rm) => rm.floor),
                    backgroundColor: [
                      'rgba(43,63,229,0.8)',
                      'rgba(250,192,19,0.8)',
                      'rgba(253,135,135,0.8)'
                    ],
                  },
                ]
              }}
            />
          </div>

        </div>

      </div>
    </div>
  )
}

export default Home