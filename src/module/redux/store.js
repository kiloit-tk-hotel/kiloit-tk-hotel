import { configureStore } from "@reduxjs/toolkit";
import authSlice from "../auth/core/authSlice";
import userSlice from "../user/core/userSlice";
import roleSlice from "../role/core/roleSlice";
import roomSlice from "../room/core/roomSlice";
import roomManagementSlice from "../roomManagement/core/roomManagementSlice";
import endUserSlice from "../endUser/core/endUserSlice";
import adminSlice from "../admin/core/adminSlice";
import paymentSlice from "../payment/core/paymentSlice";
const store = configureStore({
  reducer: {
    auth: authSlice,
    user: userSlice,
    role: roleSlice,
    roomType: roomSlice,
    roomManagement: roomManagementSlice,
    endUser: endUserSlice,
    admin: adminSlice,
    payment: paymentSlice,
  },
});

export default store;
