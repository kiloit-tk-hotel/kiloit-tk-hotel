import { useDispatch, useSelector } from "react-redux";
import {
  regEditBookingById,
  regGetPaging,
  regRoomBooking,
  regUserBooking,
  regViewBookingById,
  requestCancleBooking,
} from "./request";
import {
  setEndUsers,
  setListPaging,
  setParams,
  setViewEndUser,
} from "./endUserSlice";

const useEndUser = () => {
  const dispatch = useDispatch();
  const { params } = useSelector((state) => state.endUser);

  const onGetUserBooking = () => {
    regUserBooking(params)
      .then((res) => {
        dispatch(setEndUsers(res.data.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onGetPaging = () => {
    regGetPaging(params)
      .then((res) => {
        dispatch(setListPaging(res.data.paging));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onViewBookingById = (id) => {
    regViewBookingById(id)
      .then((res) => {
        dispatch(setViewEndUser(res.data.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onEditBookingById = (id, payload) => {
    regEditBookingById(id, payload)
      .then((res) => {
        dispatch(setEndUsers(res.data.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onEndUserBooking = (payload) => {
    regRoomBooking(payload)
      .then((res) => {
        onGetUserBooking();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onCancelBooking = (id) => {
    requestCancleBooking(id)
      .then(() => {
        onGetUserBooking();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handlePage = (page) => {
    dispatch(
      setParams({
        page: page,
      })
    );
  };

  const handleFilterCheckIn = (payload) => {
    dispatch(
      setParams({
        checkIn: payload,
        page: 0,
      })
    );
  };

  const handleFilterCheckOut = (payload) => {
    dispatch(
      setParams({
        checkOut: payload,
        page: 0,
      })
    );
  };

  const filterStatus = (status) => {
    if (status === "ALL") {
      dispatch(
        setParams({
          status: "",
          page: 0,
        })
      );
    } else if (status === "CONFIRMED") {
      dispatch(
        setParams({
          status: "CONFIRMED",
          page: 0,
        })
      );
    } else if (status === "CANCELLED") {
      dispatch(
        setParams({
          status: "CANCELLED",
          page: 0,
        })
      );
    } else if (status === "CHECKED_IN") {
      dispatch(
        setParams({
          status: "CHECKED_IN",
          page: 0,
        })
      );
    } else if (status === "CHECKED_OUT") {
      dispatch(
        setParams({
          status: "CHECKED_OUT",
          page: 0,
        })
      );
    } else {
      dispatch(
        setParams({
          status: "",
          page: 0,
        })
      );
    }
  };

  return {
    onGetUserBooking,
    onViewBookingById,
    onEditBookingById,
    onEndUserBooking,
    onCancelBooking,
    handleFilterCheckIn,
    handleFilterCheckOut,
    handlePage,
    filterStatus,
    onGetPaging,
  };
};

export default useEndUser;
