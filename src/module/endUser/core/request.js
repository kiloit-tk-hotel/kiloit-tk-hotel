import axios from "axios";

const regUserBooking = (params) => {
  return axios.get("api/booking/my-booking", {
    params: params,
  });
};

const regGetPaging = (params) => {
  return axios.get("api/booking/my-booking", {
    params: params,
  });
};

// get request view booking by id
const regViewBookingById = (id) => {
  return axios.get(`api/booking/${id}`);
};

//get request edit booking by id
const regEditBookingById = (id, payload = {}) => {
  return axios.put(`api/booking/${id}`, payload);
};

// get request booking room
const regRoomBooking = (payload = {}) => {
  return axios.post("api/booking", payload);
};

//get request cancle booking room

const requestCancleBooking = (id) => {
  return axios.post(`api/booking/${id}/cancel`);
};

export {
  regUserBooking,
  regViewBookingById,
  regEditBookingById,
  regRoomBooking,
  requestCancleBooking,
  regGetPaging,
};
