import { createSlice } from "@reduxjs/toolkit";

const endUserSlice = createSlice({
  name: "endUser",
  initialState: {
    endUsers: [],
    viewEndUser: {},
    loading: false,
    ListPaging: {},
    params: {
      page: 1,
      size: 3,
      order: "asc",
      sort: "id",
      checkIn: "",
      checkOut: "",
      status: "",
    },
  },
  reducers: {
    setEndUsers: (state, action) => {
      state.endUsers = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setViewEndUser: (state, action) => {
      state.viewEndUser = action.payload;
    },
    setParams: (state, action) => {
      state.params = {
        ...state.params,
        ...action.payload,
      };
    },
    setListPaging: (state, action) => {
      state.ListPaging = action.payload;
    },
  },
});

export const {
  setEndUsers,
  setLoading,
  setViewEndUser,
  setParams,
  setListPaging,
} = endUserSlice.actions;
export default endUserSlice.reducer;
