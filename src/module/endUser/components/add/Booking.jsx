import { Button, Label, Select, TextInput } from 'flowbite-react'
import { useFormik } from 'formik'
import React, { useEffect } from 'react'
import useRoomType from '../../../room/core/hook'
import { useSelector } from 'react-redux'
import useEndUser from '../../core/hook'

const Booking = () => {

  const { onGetRoomType } = useRoomType()
  const { onEndUserBooking } = useEndUser()
  const { roomTypes } = useSelector((state) => state.roomType)

  const formik = useFormik({
    initialValues: {
      checkIn: "",
      checkOut: "",
      specialRequest: "",
      bookingDetail: [
        { roomTypeId: "", adult: "", child: "" }
      ]
    },
    onSubmit: (values) => {
      onEndUserBooking(values)
      formik.resetForm();
    },

  });

  useEffect(() => {
    onGetRoomType()
  }, [])
  return (
    <>
      <div className="p-4">
        <form onSubmit={formik.handleSubmit}>
          <div className="grid gap-5">
            <div>
              <div className="mb-2 block">
                <Label value="Title" />
              </div>
              <TextInput name="checkIn" type="date" value={formik.values.checkIn}
                onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="checkOut" />
              </div>
              <TextInput name="checkOut" type="date" value={formik.values.checkOut}
                onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="specialRequest" />
              </div>
              <TextInput name="specialRequest" type="text" value={formik.values.specialRequest}
                onChange={formik.handleChange} required />
            </div>
            {formik.values.bookingDetail.map((item, index) => (
              <div key={index}>
                <div className="mb-2 block">
                  <Label value="roomTypeId" />
                  <Select
                    name={`bookingDetail.${index}.roomTypeId`}
                    onChange={formik.handleChange}
                    value={item.roomTypeId}
                    required
                  >
                    {roomTypes.map((roomType) => (
                      <option key={roomType.id} value={roomType.id}>
                        {roomType.id}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="mb-2 block">
                  <Label value="adult" />
                  <TextInput
                    name={`bookingDetail.${index}.adult`}
                    type="text"
                    value={item.adult}
                    onChange={formik.handleChange}
                    required
                  />
                </div>
                <div className="mb-2 block">
                  <Label value="child" />
                  <TextInput
                    name={`bookingDetail.${index}.child`}
                    type="text"
                    value={item.child}
                    onChange={formik.handleChange}
                    required
                  />
                </div>
              </div>
            ))}
          </div>

          <Button outline gradientDuoTone="purpleToBlue" type="submit" className=" mt-4">Submit</Button>
        </form>

      </div >
    </>
  )
}

export default Booking