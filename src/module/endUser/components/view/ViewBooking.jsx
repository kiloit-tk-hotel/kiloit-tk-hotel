import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import useEndUser from '../../core/hook'
import { useSelector } from 'react-redux'
import { BsViewStacked } from 'react-icons/bs'
import moment from 'moment'

const ViewBooking = () => {

  const { onViewBookingById } = useEndUser()
  const { viewEndUser } = useSelector((state) => state.endUser)
  const { bookingDetail = [] } = viewEndUser
  const { id } = useParams()


  useEffect(() => {
    onViewBookingById(id)
  }, [])


  return (
    <div>
      <p>ID : {viewEndUser.id}</p>
      <p>Status of Room : {viewEndUser.status}</p>
      <p>Check In At : {moment(viewEndUser.checkInAt).format("MMM Do YYYY")}</p>
      <p>Check Out At : {moment(viewEndUser.checkOutAt).format("MMM Do YYYY")}</p>
      <p>Special request : {viewEndUser.specialRequest}</p>
      <div>
        {
          bookingDetail.map((item, index) => {
            return (
              <div key={index}>
                <p>Room Type id : {item.id}</p>
                <p>Adult : {item.adult}</p>
                <p>Child : {item.child}</p>
                <p>Room's Name : {item.roomType.title}</p>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}

export default ViewBooking