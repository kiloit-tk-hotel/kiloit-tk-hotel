import React, { useEffect } from 'react'
import useEndUser from '../../core/hook'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Formik, useFormik } from 'formik'
import useRoomType from '../../../room/core/hook'
import { Button, Label, Select, TextInput } from 'flowbite-react'
import Swal from 'sweetalert2'

const EditBooking = () => {
  const { id } = useParams()
  const { onViewBookingById, onEditBookingById } = useEndUser()
  const { viewEndUser } = useSelector((state) => state.endUser)
  const { roomTypes } = useSelector((state) => state.roomType)

  const {bookingDetail = []} = viewEndUser;
  const roomTypeID = bookingDetail.map((item) => {
    return item.roomTypeId
  
  })

  const adult = bookingDetail.map((item) => {
    return item.adult
    
  })

  const child = bookingDetail.map((item) => {
    return  item.child
    
  })

  const { onGetRoomType } = useRoomType()

  useEffect(() => {
    onViewBookingById(id)
    onGetRoomType()
  }, [id])

  const formik = useFormik({
    initialValues: {
      checkIn: viewEndUser.checkInAt,
      checkOut: viewEndUser.checkOutAt,
      specialRequest: viewEndUser.specialRequest,
      bookingDetail: [
        { roomTypeId: roomTypeID, adult: adult , child:  child}
      ]
    },
    enableReinitialize: true,
    onSubmit: (values) => {
      Swal.fire({
        position: "top-end",
        height: "100px",
        icon: "success",
        title: "Add Successed",
        showConfirmButton: false,
        timer: 1500,
      })
        .then(() => {
          onEditBookingById(id, values);
        })
        .catch((error) => {
          Swal.fire({
            title: "Error!",
            text: "Failed to update room type",
            icon: "error",
          });
        });
    },
  })
  return (
    <div className='p-4'>
      <h1 className='text-xl mb-4 text-center'>Edit Booking</h1>
      {/* <form onSubmit={formik.handleSubmit}>
        <div className="grid md:grid-cols-2 gap-4">
          <label></label>
          <input type="date" name='checkIn' onChange={formik.handleChange} value={formik.values.checkIn} />
        </div>

        <div>
          <label></label>
          <input type="date"  name='checkOut' value={formik.values.checkOut} onChange={formik.handleChange} />
        </div>

        <div>
          <label htmlFor=""></label>
          <input type="text"  name='specialRequest' value={formik.values.specialRequest} onChange={formik.handleChange} />
        </div>

        <div>
          {formik.values.bookingDetail.map((detail, index) => (
            <div key={index}>
              <label>Room Type ID:</label>
              <select 
                name={`bookingDetail[${index}].roomTypeId`} 
                value={detail.roomTypeId} 
                onChange={formik.handleChange} 
                >
                {roomTypes.map(roomTypeId => (
                  <option key={roomTypeId} value={roomTypeId.id}>{roomTypeId.id}</option>
                ))}
              </select>

              <label>Adults:</label>
              <input 
                type="text" 
                name={`bookingDetail[${index}].adult`} 
                value={detail.adult} 
                onChange={formik.handleChange} 
              />

              <label>Children:</label>
              <input 
                type="text" 
                name={`bookingDetail[${index}].child`} 
                value={detail.child} 
                onChange={formik.handleChange} 
              />
            </div>
          ))}
        </div>

        <div className='mt-2 block'>
          <Button type='submit'>Edit</Button>
        </div>
      </form> */}
       <form onSubmit={formik.handleSubmit}>
          <div className="grid gap-5">
            <div>
              <div className="mb-2 block">
                <Label value="checkIn" />
              </div>
              <TextInput name="checkIn" type="text" value={formik.values.checkIn}
                onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="checkOut" />
              </div>
              <TextInput name="checkOut" type="text" value={formik.values.checkOut}
                onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="specialRequest" />
              </div>
              <TextInput name="specialRequest" type="text" value={formik.values.specialRequest}
                onChange={formik.handleChange} required />
            </div>
            {formik.values.bookingDetail.map((item, index) => (
              <div key={index}>
                <div className="mb-2 block">
                  <Label value="roomTypeId" />
                  <Select
                    name={`bookingDetail.${index}.roomTypeId`}
                    onChange={formik.handleChange}
                    value={item.roomTypeId}
                    required
                  >
                    {roomTypes.map((roomType) => (
                      <option key={roomType.id} value={roomType.id}>
                        {roomType.id}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="mb-2 block">
                  <Label value="adult" />
                  <TextInput
                    name={`bookingDetail.${index}.adult`}
                    type="text"
                    value={item.adult}
                    onChange={formik.handleChange}
                    required
                  />
                </div>
                <div className="mb-2 block">
                  <Label value="child" />
                  <TextInput
                    name={`bookingDetail.${index}.child`}
                    type="text"
                    value={item.child}
                    onChange={formik.handleChange}
                    required
                  />
                </div>
              </div>
            ))}
          </div>

          <Button outline gradientDuoTone="purpleToBlue" type="submit" className=" mt-4">Submit</Button>
        </form>
    </div>
  )
}

export default EditBooking