import React, { useEffect } from "react";
import useEndUser from "./core/hook";
import { useSelector } from "react-redux";
import moment from "moment";
import {
  Badge,
  Button,
  Checkbox,
  Label,
  Pagination,
  Select,
  Table,
} from "flowbite-react";
import { Link } from "react-router-dom";

const DisplayUserBooking = () => {
  const {
    onGetUserBooking,
    onCancelBooking,
    handleFilterCheckIn,
    handleFilterCheckOut,
    handlePage,
    filterStatus,
    onGetPaging,
  } = useEndUser();
  const { endUsers, params, ListPaging } = useSelector(
    (state) => state.endUser
  );
  const { page } = params;
  const totalPage = ListPaging.totalPage || 0;
  useEffect(() => {
    onGetUserBooking();
    onGetPaging();
  }, [params, page]);

  return (
    <>
      <div className="shadow p-5">
        <div className="flex justify-between items-center my-5">
          <span className="text-md font-medium">Booking List</span>
          <Link to="/user/booking">
            <Button outline gradientDuoTone="purpleToBlue">
              Create Booking
            </Button>
          </Link>
        </div>

        <div className="flex gap-9 items-center my-5">
          <div className="max-w-md">
            <div className="mb-2 block">
              <Label htmlFor="countries" value="Filter By CheckIn Date" />
            </div>
            <Select
              id="countries"
              required
              onChange={(e) => handleFilterCheckIn(e.target.value)}
              value={params.checkInAt}
            >
              {endUsers.map((item) => (
                <option
                  value={new Date(item.checkInAt).toLocaleDateString("en-CA")}
                >
                  {new Date(item.checkInAt).toLocaleDateString("en-CA")}
                </option>
              ))}
              <option value="">All</option>
            </Select>
          </div>

          <div className="max-w-md">
            <div className="mb-2 block">
              <Label htmlFor="countries" value="Filter By CheckOut Date" />
            </div>
            <Select
              id="countries"
              required
              onChange={(e) => handleFilterCheckOut(e.target.value)}
              value={params.checkOutAt}
            >
              {endUsers.map((item) => (
                <option
                  value={new Date(item.checkOutAt).toLocaleDateString("en-CA")}
                >
                  {new Date(item.checkOutAt).toLocaleDateString("en-CA")}
                </option>
              ))}
              <option value="">All</option>
            </Select>
          </div>
        </div>

        <div className="mb-5">
          <div className="max-w-md">
            <div className="mb-2 block">
              <Label htmlFor="countries" value="Filter By Status" />
            </div>
            <Select
              id="countries"
              required
              onChange={(e) => filterStatus(e.target.value)}
              value={params.status || "ALL"}
            >
              <option value="CONFIRMED">CONFIRMED</option>
              <option value="CANCELLED">CANCELLED</option>
              <option value="CHECKED_IN">CHECKED_IN</option>
              <option value="CHECKED_OUT">CHECKED_OUT</option>
              <option value="ALL">All</option>
            </Select>
          </div>
        </div>

        <div className="overflow-x-auto shadow">
          <Table hoverable>
            {/* Table Head */}
            <Table.Head>
              <Table.HeadCell>checkbox</Table.HeadCell>
              <Table.HeadCell>ID</Table.HeadCell>
              <Table.HeadCell>Name</Table.HeadCell>
              <Table.HeadCell>Status</Table.HeadCell>
              <Table.HeadCell>Check In</Table.HeadCell>
              <Table.HeadCell>Chek Out</Table.HeadCell>
              <Table.HeadCell>
                <span className="sr-only">View</span>
              </Table.HeadCell>
              <Table.HeadCell>
                <span className="sr-only">Delete</span>
              </Table.HeadCell>
              <Table.HeadCell>
                <span className="sr-only">Update </span>
              </Table.HeadCell>
            </Table.Head>
            {/* Table Body */}
            <Table.Body className="divide-y">
              {/* Mapping over endUsers to create rows */}
              {endUsers?.map((user) => (
                <Table.Row
                  key={user.id}
                  className="bg-white dark:border-gray-700 dark:bg-gray-800"
                >
                  {/* Checkbox */}
                  <Table.Cell className="p-4">
                    <Checkbox />
                  </Table.Cell>
                  {/* Booking ID */}
                  <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                    {user.id}
                  </Table.Cell>
                  {/* User Booking Name */}
                  <Table.Cell>{user.user.name}</Table.Cell>
                  {/* Booking Status */}
                  <Table.Cell>{user.status}</Table.Cell>
                  {/* Check-In Date */}
                  <Table.Cell>
                    {moment(user.checkInAt).format("MMM Do YYYY")}
                  </Table.Cell>
                  {/* Check-Out Date */}
                  <Table.Cell>
                    {moment(user.checkOutAt).format("MMM Do YYYY")}
                  </Table.Cell>
                  {/* View Booking */}
                  <Table.Cell>
                    <Link
                      to={`/user/view/booking/${user.id}`}
                      className="font-medium text-cyan-600 hover:underline dark:text-sky-500"
                    >
                      <Badge
                        color="success"
                        className="flex items-center justify-center"
                      >
                        View
                      </Badge>
                    </Link>
                  </Table.Cell>
                  {/* Delete Booking */}
                  <Table.Cell>
                    <span className="font-medium text-red-600 hover:underline decor cursor-pointer">
                      <Badge
                        color="failure"
                        className="flex items-center justify-center"
                        onClick={() => onCancelBooking(user.id)}
                      >
                        Cancel
                      </Badge>
                    </span>
                  </Table.Cell>
                  {/* Edit Booking */}
                  <Table.Cell>
                    <Link
                      to={`/view/booking/edit/${user.id}`}
                      className="font-medium text-green-500-600 hover:underline decor cursor-pointer"
                    >
                      <Badge
                        color="purple"
                        size="md"
                        className="flex items-center justify-center"
                      >
                        Edit
                      </Badge>
                    </Link>
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        </div>
        <div className="flex overflow-x-auto sm:justify-end">
          <Pagination
            currentPage={page}
            totalPages={totalPage}
            onPageChange={(page) => handlePage(page)}
          />
        </div>
      </div>
    </>
  );
};

export default DisplayUserBooking;
