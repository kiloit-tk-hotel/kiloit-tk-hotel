import React from "react";
import { Outlet } from "react-router-dom";
import SideBar from "../../components/sidebar/SideBar";
import NavBar from "../../components/header/NavBar";
import { useState } from "react";

const Layout = () => {
  const [open, setOpen] = useState(true);

  return (
    <>
      <div>
        <div className={`flex w-full`}>
          <div className={`${open ? "w-80" : "w-20"}`}>
            <SideBar open={open} setOpen={setOpen} />
          </div>

          <div className="overflow-x-auto w-full">
            <div>
              <NavBar />
            </div>

            <main className="overflow-x-auto">
              <Outlet />
            </main>
          </div>
        </div>
      </div>
    </>
  );
};

export default Layout;
