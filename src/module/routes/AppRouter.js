import React from "react";
import { useSelector } from "react-redux";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Layout from "../layout/Layout";
import LoginForm from "../auth";
import Home from "../home/Home";
import DisplayUser from "../user";
import Edit from "../user/components/update/Edit";
import AddUser from "../user/components/add/AddUser";
import ViewUser from "../user/components/view/ViewUser";
import DisplayProfile from "../auth/components/DisplayProfile/DisplayProfile";
import UpdateProfile from "../auth/components/UpdateProfile/UpdateProfile";
import DisplayRoleUser from "../role/components/dispalyRoleUser/DisplayRoleUser";
import View from "../role/component/View";
import AddRole from "../role/component/AddRole";
import DisplayRoomType from "../room";
import AddRoomType from "../room/components/addRoom/AddRoomType";
import ViewRoomBYId from "../room/components/view/ViewRoomBYId";
import DisplayRoomManagement from "../roomManagement";
import EditRoomType from "../room/components/Edit/EditRoomType";
import ViewRoom from "../roomManagement/components/view/ViewRoom";
import EditRoomManagement from "../roomManagement/components/update/EditRoomManagement";
import ResetPassword from "../auth/components/ResetPassword/ResetPassword";
import Post from "../roomManagement/components/post/Post";
import DisplayUserBooking from "../endUser";
import Booking from "../endUser/components/add/Booking";
import ViewBooking from "../endUser/components/view/ViewBooking";
import EditBooking from "../endUser/components/edit/EditBooking";
import GetAllBooking from "../admin";
import AdminViewBooking from "../admin/components/view/AdminViewBooking";
import UpdateBooking from "../admin/components/update/UpdateBooking";
import AdminBooking from "../admin/components/booking/AdminBooking";
import ViewPayment from "../payment/components/view/ViewPayment";
import EditPayment from "../payment/components/update/EditPayment";
import UserRegister from "../auth/components/UserRegister/UserRegister";
import Update from "../roomManagement/components/update/EditRoomManagement";
import UpdatePermission from "../role/component/UpdatePermission";
import UpdateRole from "../role/component/UpdateRole";

const AppRouter = () => {
  const { token } = useSelector((state) => state.auth);

  const router = createBrowserRouter([
    {
      path: "/",
      element: token ? <Layout /> : <LoginForm />,
      children: token
        ? [
            { path: "/", element: <Home />, index: true },
            { path: "user", element: <DisplayUser /> },
            { path: "edit/:id", element: <Edit /> },
            { path: "addUser", element: <AddUser /> },
            { path: "view/:id", element: <ViewUser /> },
            { path: "profile", element: <DisplayProfile /> },
            { path: "profile/update", element: <UpdateProfile /> },
            { path: "role", element: <DisplayRoleUser /> },
            { path: "viewRole/:id", element: <View /> },
            { path: "addrole", element: <AddRole /> },
            { path: "updateRole/:id", element: <UpdateRole /> },
            { path: "roomType", element: <DisplayRoomType /> },
            { path: "room/add", element: <AddRoomType /> },
            { path: "viewRoom/:id", element: <ViewRoomBYId /> },
            { path: "roomManagement", element: <DisplayRoomManagement /> },
            { path: "update/:id", element: <Update /> },
            { path: "editRoomType/:id", element: <EditRoomType /> },
            { path: "viewRoomManagement/:id", element: <ViewRoom /> },
            {
              path: "updateRoomManagement/:id",
              element: <EditRoomManagement />,
            },
            { path: "resetPassword", element: <ResetPassword /> },
            { path: "addRoomManagement", element: <Post /> },
            { path: "displayUserBooking", element: <DisplayUserBooking /> },
            { path: "user/booking", element: <Booking /> },
            { path: "user/view/booking/:id", element: <ViewBooking /> },
            { path: "view/booking/edit/:id", element: <EditBooking /> },
            { path: "getAllBooking", element: <GetAllBooking /> },
            { path: "admin/viewBooking/:id", element: <AdminViewBooking /> },
            { path: "view/admin/update/:id", element: <UpdateBooking /> },
            { path: "admin/booking", element: <AdminBooking /> },
            { path: "view/payment/:id", element: <ViewPayment /> },
            { path: "edit/payment/:id", element: <EditPayment /> },
            { path: "edit/permission/:id", element: <UpdatePermission /> },
          ]
        : [],
    },
    { path: "*", element: <LoginForm /> },
    { path: "signup", element: <UserRegister /> },
  ]);

  return <RouterProvider router={router} />;
};

export default AppRouter;
