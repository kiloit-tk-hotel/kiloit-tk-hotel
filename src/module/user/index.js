import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import {
  Badge,
  Button,
  Checkbox,
  Pagination,
  Table,
  TextInput,
} from "flowbite-react";
import { TiArrowSortedDown } from "react-icons/ti";
import { FiSearch } from "react-icons/fi";
import { useUser } from "./core/hook";
import Component from "../../components/spinner";
import Swal from "sweetalert2";

const DisplayUser = () => {
  const { onGetUser, onDeleteUser, onSearch, onPaging, onHandlePage } =
    useUser();
  const { users, loading, params, paging } = useSelector((state) => state.user);
  const totalPage = paging.totalPage || 1;
  const { page } = params;
  useEffect(() => {
    onGetUser();
    onPaging();
  }, [params]);

  const handleDelete = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        onDeleteUser(id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  };

  return (
    <div className="shadow p-5">
      {loading ? (
        <>
          <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
            <Component />
          </div>
        </>
      ) : (
        <>
          <div className="mb-5 flex justify-between items-center">
            <span className="text-md font-medium"></span>
            <Link to="/addUser">
              <Button outline gradientDuoTone="purpleToBlue">
                Create User
              </Button>
            </Link>
          </div>

          <div className="my-5">
            <TextInput
              icon={FiSearch}
              onChange={(e) => onSearch(e.target.value)}
            />
          </div>
          <div className="overflow-x-auto shadow">
            <Table hoverable>
              <Table.Head>
                <Table.HeadCell>Checkbox</Table.HeadCell>
                <Table.HeadCell className="flex gap-2 items-center">
                  <p>ID</p>
                  <TiArrowSortedDown size={20} />
                </Table.HeadCell>
                <Table.HeadCell>Username</Table.HeadCell>
                <Table.HeadCell>Name</Table.HeadCell>
                <Table.HeadCell>Email</Table.HeadCell>
                <Table.HeadCell>Phone</Table.HeadCell>
                <Table.HeadCell>Address</Table.HeadCell>
                <Table.HeadCell>Bio</Table.HeadCell>
                <Table.HeadCell>Avatar</Table.HeadCell>

                <Table.HeadCell>
                  <span className="sr-only">View</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Delete</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Update</span>
                </Table.HeadCell>
              </Table.Head>
              <Table.Body className="divide-y">
                {users.map((s) => {
                  return (
                    <Table.Row
                      key={s.id}
                      className="bg-white dark:border-gray-700 dark:bg-gray-800"
                    >
                      <Table.Cell className="p-4">
                        <Checkbox />
                      </Table.Cell>
                      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                        {s.id}
                      </Table.Cell>

                      <Table.Cell>{s.username}</Table.Cell>
                      <Table.Cell>{s.name}</Table.Cell>
                      <Table.Cell>{s.email}</Table.Cell>
                      <Table.Cell>{s.phone}</Table.Cell>
                      <Table.Cell>{s.address}</Table.Cell>
                      <Table.Cell>{s.bio}</Table.Cell>
                      <Table.Cell>
                        <img
                          className="w-16 h-16 rounded-full object-cover"
                          src={s.avatar}
                          alt=""
                        />
                      </Table.Cell>
                      <Table.Cell>{s.status}</Table.Cell>

                      <Table.Cell>
                        <Link
                          to={`/view/${s.id}`}
                          className="font-medium text-cyan-600 hover:underline dark:text-sky-500"
                        >
                          <Badge
                            color="success"
                            className="flex items-center justify-center p-0"
                          >
                            View
                          </Badge>
                        </Link>
                      </Table.Cell>
                      <Table.Cell
                        onClick={() => handleDelete(s.id)}
                        className="font-medium text-red-600 hover:underline decor cursor-pointer "
                      >
                        <Badge
                          color="failure"
                          className="flex items-center justify-center p-0"
                        >
                          Delete
                        </Badge>
                      </Table.Cell>
                      <Table.Cell className="font-medium text-green-500-600 hover:underline decor cursor-pointer">
                        <Link to={`/edit/${s.id}`}>
                          <Badge
                            color="purple"
                            size="md"
                            className="flex items-center justify-center p-0"
                          >
                            Edit
                          </Badge>
                        </Link>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
              </Table.Body>
            </Table>
          </div>
          <div className="flex overflow-x-auto sm:justify-end">
            <Pagination
              totalPages={totalPage}
              currentPage={page}
              onPageChange={(page) => onHandlePage(page)}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default DisplayUser;
