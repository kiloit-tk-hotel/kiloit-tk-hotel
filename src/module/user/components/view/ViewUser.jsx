import { useParams } from "react-router-dom";
import { useUser } from "../../core/hook";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { FaUser } from "react-icons/fa";
import { FaLocationDot } from "react-icons/fa6";
import { MdEmail } from "react-icons/md";
import { BiOutline } from "react-icons/bi";
import { GrStatusCriticalSmall } from "react-icons/gr";

const ViewUser = () => {
  const { id } = useParams();
  const { viewById } = useSelector((state) => state.user);
  const { onViewById } = useUser();

  useEffect(() => {
    onViewById(id);
  }, [id]);

  return (
      <div className="p-5 text-sky-800 font-medium ">
        <div
            className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div className="flex items-center justify-center p-5">
                <img className="rounded" src={viewById.avatar} alt=""/>
            </div>
            <div className="p-5">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white uppercase">{viewById.username}</h5>
                <div className="flex items-center justify-start">
                    <p className="pr-2"><FaUser/></p>
                    <p>Name : {viewById.name}</p>
                </div>
                <div className="flex items-center justify-start">
                    <p className="pr-2"><FaLocationDot/></p>
                    <p>Address : {viewById.address}</p>
                </div>
                <div className="flex items-center justify-start">
                    <p className="pr-2"><MdEmail/></p>
                    <p>Email : {viewById.email}</p>
                </div>
                <div className="flex items-center justify-start">
                    <p className="pr-2"><BiOutline/></p>
                    <p>Bio : {viewById.bio}</p>
                </div>
                <div className="flex items-center justify-start">
                    <p className="pr-2"><GrStatusCriticalSmall /></p>
                    <p>Status : {viewById.status}</p>
                </div>

            </div>
        </div>
      </div>
  );
};

export default ViewUser;
