import React, { useEffect } from "react";
import { useUser } from "../../core/hook";
import { useSelector } from "react-redux";
import { Button, Label, TextInput, Textarea } from "flowbite-react";
import { useFormik } from "formik";
import { useParams } from "react-router-dom";

const Edit = () => {
  const { id } = useParams();
  const { onViewById, onEditUser, onGetUser } = useUser();
  const { viewById, users } = useSelector((state) => state.user);

  const formik = useFormik({
    initialValues: viewById,
    enableReinitialize: true,
    onSubmit: (values) => {
      onEditUser(id, values);
      formik.resetForm();
    },
  });

  useEffect(() => {
    onViewById(id);
  }, [id]);

  useEffect(() => {
    onGetUser();
  }, []);

  return (
    <section>
      <form onSubmit={formik.handleSubmit} className="mx-5">
        <div className="grid grid-cols-2 gap-4 p-5 ">
          <div>
            <div className="mb-2 block">
              <Label value="Username" />
            </div>
            <TextInput
              type="text"
              sizing="md"
              name="username"
              value={formik.values.username}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Name" />
            </div>
            <TextInput
              type="text"
              sizing="md"
              name="name"
              value={formik.values.name}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Phone" />
            </div>
            <TextInput
              type="number"
              sizing="md"
              name="phone"
              value={formik.values.phone}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Email" />
            </div>
            <TextInput
              type="text"
              sizing="md"
              name="email"
              value={formik.values.email}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Password" />
            </div>
            <TextInput
              type="password"
              sizing="md"
              name="password"
              value={formik.values.password}
              onChange={formik.handleChange}
            />
          </div>

          <div>
            <div className="mb-2 block">
              <Label value="Status" />
            </div>
            <TextInput
              type="text"
              sizing="md"
              name="status"
              value={formik.values.status}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Bio" />
            </div>
            <TextInput
              type="text"
              sizing="md"
              name="bio"
              value={formik.values.bio}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="DateOfBirth" />
            </div>
            <TextInput
              type="date"
              sizing="md"
              name="dateOfBirth"
              value={formik.values.dateOfbirth}
              onChange={formik.handleChange}
            />
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="RoleId" />
            </div>
            <select
              name="roleId"
              onChange={formik.handleChange}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            >
              {users.map((role, i) => {
                return (
                  <option key={i} value={role.roleEntity.id}>
                    {role.roleEntity.id}
                  </option>
                );
              })}
            </select>
          </div>
          <div>
            <div className="mb-2 block">
              <Label value="Address" />
            </div>
            <Textarea
              type="text"
              sizing="md"
              name="address"
              value={formik.values.address}
              onChange={formik.handleChange}
            />
          </div>
          <div className="mt-2 block ">
            <Button outline gradientDuoTone="purpleToBlue" type="submit">
              Submit
            </Button>
          </div>
        </div>
      </form>
    </section>
  );
};

export default Edit;
