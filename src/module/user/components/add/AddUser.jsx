import { useFormik } from "formik";
import React from "react";
import { Button, Label, TextInput, Select, Textarea } from "flowbite-react";
import { useUser } from "../../core/hook";
import useRole from "../../../role/core/hook";
import { useEffect } from "react";
import { useSelector } from "react-redux";

const AddUser = () => {
  const { onAddUser } = useUser();
  const { onGetRole } = useRole();
  const roles = useSelector((state) => state.role.roleList);

  const formik = useFormik({
    initialValues: {
      username: "",
      name: "",
      phone: "",
      email: "",
      password: "",
      address: "",
      dateOfBirth: "",
      status: "",
      roleId: "",
      bio: "",
    },
    onSubmit: (values) => {
      onAddUser(values);
    },
  });

  useEffect(() => {
    onGetRole();
  }, []);

  return (
    <form onSubmit={formik.handleSubmit} className="mx-5">
      <div className="grid grid-cols-2 gap-4 p-5">
        <div>
          <div className="mb-2 block">
            <Label value="Username"/>
          </div>
          <TextInput
              type="text"
              sizing="md"
              name="username"
              value={formik.values.username}
              onChange={formik.handleChange}
          />
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Name"/>
          </div>
          <TextInput
              type="text"
              sizing="md"
              name="name"
              value={formik.values.name}
              onChange={formik.handleChange}
          />
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Phone"/>
          </div>
          <TextInput
              type="number"
              sizing="md"
              name="phone"
              value={formik.values.phone}
              onChange={formik.handleChange}
          />
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Email"/>
          </div>
          <TextInput
              type="text"
              sizing="md"
              name="email"
              value={formik.values.email}
              onChange={formik.handleChange}
          />
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="DOB"/>
          </div>
          <TextInput
              type="date"
              sizing="md"
              name="dateOfBirth"
              value={formik.values.dateOfBirth}
              onChange={formik.handleChange}
          />
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Password"/>
          </div>
          <TextInput
              type="password"
              sizing="md"
              name="password"
              value={formik.values.password}
              onChange={formik.handleChange}
          />
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Status"/>
          </div>
          <Select
              name="status"
              value={formik.values.status}
              onChange={formik.handleChange}
          >
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
          </Select>
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Role"/>
          </div>
          <select
              name="roleId"
              onChange={formik.handleChange}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          >
            {roles.map((role, i) => {
              return (
                  <option key={i} value={role.id}>
                    {role.name}
                  </option>
              );
            })}
          </select>
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Bio"/>
          </div>
          <TextInput
              type="text"
              sizing="md"
              name="bio"
              value={formik.values.bio}
              onChange={formik.handleChange}
          />
        </div>
        <div>
          <div className="mb-2 block">
            <Label value="Address"/>
          </div>
          <Textarea
              type="text"
              sizing="md"
              name="address"
              value={formik.values.address}
              onChange={formik.handleChange}
          />
        </div>
        <div></div>
        <div className="mt-2 block ">
          <Button outline gradientDuoTone="purpleToBlue" type="submit">
            Submit
          </Button>
        </div>
      </div>
    </form>
  );
};

export default AddUser;
