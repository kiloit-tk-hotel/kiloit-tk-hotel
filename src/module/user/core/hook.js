import { useDispatch, useSelector } from "react-redux";
import {
  regAddUser,
  regDeleteUser,
  regEditUser,
  regUser,
  regViewUserById,
  reqPaging,
} from "./reguest";
import {
  setAddUser,
  setDeleteUser,
  setUsers,
  setEditUser,
  setViewById,
  setParams,
  setPaging,
} from "./userSlice";
import { setLoading } from "../../role/core/roleSlice";

const useUser = () => {
  const dispatch = useDispatch();
  const { params } = useSelector((state) => state.user);

  const onGetUser = () => {
    dispatch(setLoading(true));
    regUser(params).then((res) => {
      dispatch(setUsers(res.data.data));
    });
    dispatch(setLoading(false));
  };

  const onAddUser = (payload) => {
    dispatch(setLoading(true));
    regAddUser(payload)
      .then((res) => {
        console.log(res.data.data);
        dispatch(setAddUser(res.data.data));
      })
      .catch((err) => {
        console.log(err);
      });
    dispatch(setLoading(false));
  };

  const onDeleteUser = (id) => {
    dispatch(setLoading(true));
    regDeleteUser(id).then(() => {
      dispatch(setDeleteUser(id));
    });
    dispatch(setLoading(false));
  };

  const onViewById = (id) => {
    dispatch(setLoading(true));
    regViewUserById(id).then((res) => {
      dispatch(setViewById(res.data.data));
    });
    dispatch(setLoading(false));
  };

  const onEditUser = (id, payload) => {
    dispatch(setLoading(true));
    regEditUser(id, payload).then((res) => {
      dispatch(setEditUser(res.data.data));
    });
    dispatch(setLoading(false));
  };

  const onSearch = (text) => {
    dispatch(
      setParams({
        query: text,
        page: 1,
      })
    );
  };

  const onHandlePage = (page) => {
    dispatch(
      setParams({
        page: page,
      })
    );
  };

  const onPaging = () => {
    reqPaging(params)
      .then((res) => {
        dispatch(setPaging(res.data.paging));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // const handleSort = (field) => {
  //   const newOrder = sort === field && order === "asc" ? "desc" : "asc";
  //   dispatch(setParams({ sort: field, order: newOrder }));
  // };

  // const onPaging = () => {
  //   regRoomManagement(params).then((res) => {
  //     dispatch(setPaging(res.data.paging));
  //   });
  // };

  return {
    onGetUser,
    onAddUser,
    onDeleteUser,
    onEditUser,
    onViewById,
    onSearch,
    onHandlePage,
    onPaging,
  };
};

export { useUser };
