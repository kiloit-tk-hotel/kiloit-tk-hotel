import axios from "axios";

const regUser = (params) => {
  return axios.get("api/users", {
    params: params,
  });
};

const regAddUser = (payload = {}) => {
  return axios.post("api/users", payload);
};

const regDeleteUser = (id) => {
  return axios.delete(`api/users/${id}`);
};

const regViewUserById = (id) => {
  return axios.get(`api/users/${id}`);
};

const regEditUser = (id, payload = {}) => {
  return axios.put(`api/users/${id}`, payload);
};

const reqPaging = (params) => {
  return axios.get("api/users", {
    params: params,
  });
};

export {
  regUser,
  regAddUser,
  regDeleteUser,
  regEditUser,
  regViewUserById,
  reqPaging,
};
