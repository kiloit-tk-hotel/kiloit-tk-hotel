import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "user",
  initialState: {
    users: [],
    addUser: [],
    viewById: {},
    loading: false,
    totalPage: 0,
    paging: {},
    params: {
      query: "",
      page: 1,
      size: 12,
      order: "desc",
      sort: "id",
    },
  },
  reducers: {
    setUsers: (state, action) => {
      state.users = action.payload;
    },
    setAddUser: (state, action) => {
      state.addUser = action.payload;
    },
    setDeleteUser: (state, action) => {
      state.users = state.users.filter((u) => u.id !== action.payload);
    },
    setViewById: (state, action) => {
      state.viewById = action.payload;
    },
    setLoadingState: (state, action) => {
      state.loading = action.payload;
    },
    setParams: (state, action) => {
      state.params = {
        ...state.params,
        ...action.payload,
      };
    },
    setPaging: (state, action) => {
      state.paging = action.payload;
    },
  },
});

export const {
  setUsers,
  setAddUser,
  setDeleteUser,
  setEditUser,
  setViewById,
  setLoadingState,
  setParams,
  setPaging,
} = userSlice.actions;

export default userSlice.reducer;
