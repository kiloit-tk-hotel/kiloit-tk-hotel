import {
  Badge,
  Button,
  Checkbox,
  Dropdown,
  Label,
  Pagination,
  Select,
  Table,
} from "flowbite-react";
import useAdmin from "./core/hook";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { log10 } from "chart.js/helpers";

const GetAllBooking = () => {
  const {
    onGetAllBooking,
    onCancelBooking,
    onDelteBooking,
    onHandlePage,
    handleSort,
    handleFilter,
    filterStatus,
    onCheckIn,
    onCheckOut,
    onPaging,
  } = useAdmin();
  const { allBooking, params, paging } = useSelector((state) => state.admin);
  const totalPage = paging.totalPage || 0;
  const { page } = params;

  const handleDelte = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        onDelteBooking(id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  };

  useEffect(() => {
    onGetAllBooking();
    onPaging();
  }, [params]);

  return (
    <>
      <div className="shadow p-5">
        <div className="flex justify-between items-center my-5">
          <span className="text-md font-medium">Booking List</span>
          <Link to="/admin/booking">
            <Button outline gradientDuoTone="purpleToBlue">
              Create Booking
            </Button>
          </Link>
        </div>

        <div className="flex gap-9 items-center my-5">
          <div className="max-w-md">
            <div className="mb-2 block">
              <Label htmlFor="countries" value="Filter By CheckIn Date" />
            </div>
            <Select
              id="countries"
              required
              onChange={(e) => handleFilter("checkIn", e.target.value)}
              value={
                allBooking.length > 0
                  ? new Date(allBooking[0].checkInAt).toLocaleDateString(
                      "en-CA"
                    )
                  : null
              }
            >
              {allBooking.map((item) => (
                <option
                  value={new Date(item.checkInAt).toLocaleDateString("en-CA")}
                >
                  {new Date(item.checkInAt).toLocaleDateString("en-CA")}
                </option>
              ))}
              <option value="">All</option>
            </Select>
          </div>

          <div className="max-w-md">
            <div className="mb-2 block">
              <Label htmlFor="countries" value="Filter By CheckOut Date" />
            </div>
            <Select
              id="countries"
              required
              onChange={(e) => handleFilter("checkOut", e.target.value)}
              value={
                allBooking.length > 0
                  ? new Date(allBooking[0].checkOutAt).toLocaleDateString(
                      "en-CA"
                    )
                  : ""
              }
            >
              {allBooking.map((item) => (
                <option
                  value={new Date(item.checkOutAt).toLocaleDateString("en-CA")}
                >
                  {new Date(item.checkOutAt).toLocaleDateString("en-CA")}
                </option>
              ))}
              <option value="">All</option>
            </Select>
          </div>
        </div>

        <div className="mb-5">
          <div className="max-w-md">
            <div className="mb-2 block">
              <Label htmlFor="countries" value="Filter By Status" />
            </div>
            <Select
              id="countries"
              required
              onChange={(e) => filterStatus(e.target.value)}
              value={allBooking.length > 0 ? allBooking[0].status : ""}
            >
              <option value="CONFIRMED">CONFIRMED</option>
              <option value="CANCELLED">CANCELLED</option>
              <option value="CHECKED_IN">CHECKED_IN</option>
              <option value="CHECKED_OUT">CHECKED_OUT</option>
              <option value="ALL">All</option>
            </Select>
          </div>
        </div>
        <div className="overflow-auto shadow">
          <Table hoverable>
            <Table.Head>
              <Table.HeadCell>checkbox</Table.HeadCell>
              <Table.HeadCell onClick={() => handleSort("id")}>
                Id
              </Table.HeadCell>
              <Table.HeadCell>Status</Table.HeadCell>
              <Table.HeadCell>checkInAt</Table.HeadCell>
              <Table.HeadCell>checkOutAt</Table.HeadCell>
              <Table.HeadCell>specialRequest</Table.HeadCell>
              <Table.HeadCell>
                <span className="">Action</span>
              </Table.HeadCell>
              <Table.HeadCell>
                <span className="sr-only">View</span>
              </Table.HeadCell>
              <Table.HeadCell>
                <span className="sr-only">Delete</span>
              </Table.HeadCell>
              <Table.HeadCell>
                <span className="sr-only">Update</span>
              </Table.HeadCell>
            </Table.Head>
            <Table.Body className="divide-y">
              {allBooking.map((b) => {
                return (
                  <Table.Row
                    key={b.id}
                    className="bg-white dark:border-gray-700 dark:bg-gray-800"
                  >
                    <Table.Cell className="p-4">
                      <Checkbox />
                    </Table.Cell>
                    <Table.Cell>{b.id}</Table.Cell>
                    <Table.Cell>{b.status}</Table.Cell>
                    <Table.Cell>
                      {new Date(b.checkInAt).toLocaleDateString("en-CA")}
                    </Table.Cell>
                    <Table.Cell>
                      {new Date(b.checkOutAt).toLocaleDateString("en-CA")}
                    </Table.Cell>
                    <Table.Cell>{b.specialRequest}</Table.Cell>

                    <Table.Cell className="font-medium hover:underline decor  cursor-pointer">
                      <Dropdown
                        label="Action"
                        outline
                        dismissOnClick={false}
                        size={"sm"}
                        className="text-xs"
                      >
                        <Dropdown.Item
                          className="text-xs"
                          onClick={() => onCancelBooking(b.id)}
                        >
                          CANCELLED
                        </Dropdown.Item>
                        <Dropdown.Item
                          className="text-xs"
                          onClick={() => onCheckIn(b.id)}
                        >
                          CHECKED_IN
                        </Dropdown.Item>
                        <Dropdown.Item
                          className="text-xs"
                          onClick={() => onCheckOut(b.id)}
                        >
                          CHECKED_OUT
                        </Dropdown.Item>
                      </Dropdown>
                    </Table.Cell>
                    <Table.Cell>
                      <Link
                        to={`/admin/viewBooking/${b.id}`}
                        className="font-medium text-cyan-600 hover:underline dark:text-sky-500"
                      >
                        <Badge
                          color="success"
                          className="flex items-center justify-center"
                        >
                          View
                        </Badge>
                      </Link>
                    </Table.Cell>
                    <Table.Cell className="font-medium text-red-600 hover:underline decor  cursor-pointer">
                      <Badge
                        color="failure"
                        className="flex items-center justify-center"
                        onClick={() => handleDelte(b.id)}
                      >
                        Delete
                      </Badge>
                    </Table.Cell>
                    <Table.Cell className="font-medium text-green-500-600 hover:underline decor cursor-pointer ">
                      <Link to={`/view/admin/update/${b.id}`}>
                        <Badge
                          color="purple"
                          size="md"
                          className="flex items-center justify-center"
                        >
                          Edit
                        </Badge>
                      </Link>
                    </Table.Cell>
                  </Table.Row>
                );
              })}
            </Table.Body>
          </Table>
        </div>

        <div className="flex overflow-x-auto sm:justify-end">
          <Pagination
            currentPage={page}
            totalPages={totalPage}
            onPageChange={(page) => onHandlePage(page)}
          />
        </div>
      </div>
    </>
  );
};

export default GetAllBooking;
