import { createSlice } from "@reduxjs/toolkit";

const adminSlice = createSlice({
  name: "admin",
  initialState: {
    // panha
    allBooking: [],
    viewEndadmin: {},
    viewBooking: {},
    admin: [],
    totalPage: 0,
    paging: {},
    params: {
      page: 1,
      size: 12,
      order: "asc",
      sort: "id",
      checkIn: "",
      checkOut: "",
      status: "",
    },
  },
  reducers: {
    setAllBooking: (state, action) => {
      state.allBooking = action.payload;
    },
    setViewBooking: (state, action) => {
      state.viewBooking = action.payload;
    },
    setViewEndadmin: (state, action) => {
      state.viewEndadmin = action.payload;
    },
    setParams: (state, action) => {
      state.params = {
        ...state.params,
        ...action.payload,
      };
    },
    setAdmin: (state, action) => {
      state.admin = action.payload;
    },
    setPaging: (state, action) => {
      state.paging = action.payload;
    },
  },
});
export const {
  setAllBooking,
  setViewBooking,
  setParams,
  setViewEndadmin,
  setAdmin,
  setPaging,
} = adminSlice.actions;
export default adminSlice.reducer;
