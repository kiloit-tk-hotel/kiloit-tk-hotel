import { useDispatch, useSelector } from "react-redux";
import {
  regAllBooking,
  regViewAllBooking,
  regDeleteBooking,
  regCancelBooking,
  reqCheckIn,
  reqCheckOut,
  regUpdateBooking,
  regAdminBooking,
  regPaging,
} from "./request";
import {
  setAllBooking,
  setParams,
  setViewBooking,
  setPaging,
} from "./adminSlice";

const useAdmin = () => {
  const dispatch = useDispatch();

  const { params } = useSelector((state) => state.admin);
  const { sort, order } = params;

  // on get all booking
  const onGetAllBooking = () => {
    regAllBooking(params)
      .then((res) => {
        dispatch(setAllBooking(res.data.data));
        dispatch(setPaging(res.data.paging));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // on get booking by id

  const onViewBooking = (id) => {
    regViewAllBooking(id)
      .then((res) => {
        dispatch(setViewBooking(res.data.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // on booking

  // on cancel booking
  const onCancelBooking = (id) => {
    regCancelBooking(id)
      .then((res) => {
        onGetAllBooking();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // on delete booking by id
  const onDelteBooking = (id) => {
    regDeleteBooking(id).then((res) => {
      onGetAllBooking();
    });
  };

  // on update booking
  const onEditBookingById = (id, payload) => {
    regUpdateBooking(id, payload)
      .then(() => {
        onGetAllBooking();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // on check in,

  const onCheckIn = (id) => {
    reqCheckIn(id)
      .then(() => {
        onGetAllBooking();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // on check out

  const onCheckOut = (id) => {
    reqCheckOut(id)
      .then(() => {
        onGetAllBooking();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onHandlePage = (page) => {
    dispatch(
      setParams({
        page: page,
      })
    );
  };

  const handleSort = (field) => {
    const newOrder = sort === field && order === "asc" ? "desc" : "asc";
    dispatch(setParams({ sort: field, order: newOrder }));
  };

  const onSearch = (text) => {
    dispatch(
      setParams({
        query: text,
        page: 1,
      })
    );
  };

  const handleFilter = (field, value) => {
    dispatch(
      setParams({
        [field]: value,
      })
    );
  };

  const handleReset = () => {
    dispatch(setParams({}));
  };

  const filterStatus = (status) => {
    if (status === "CONFIRMED") {
      dispatch(
        setParams({
          status: status,
        })
      );
    } else if (status === "CANCELLED") {
      dispatch(
        setParams({
          status: status,
        })
      );
    } else if (status === "CHECKED_IN") {
      dispatch(
        setParams({
          status: status,
        })
      );
    } else if (status === "CHECKED_OUT") {
      dispatch(
        setParams({
          status: status,
        })
      );
    } else if (status === "ALL") {
      dispatch(
        setParams({
          status: "",
        })
      );
    } else {
      dispatch(
        setParams({
          status: "",
        })
      );
    }
  };

  const onPaging = () => {
    regPaging(params)
      .then((res) => {
        dispatch(setPaging(res.data.paging));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onAdminBooking = (payload) => {
    regAdminBooking(payload)
      .then((res) => {
        onGetAllBooking();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return {
    onGetAllBooking,
    onCancelBooking,
    onDelteBooking,
    onViewBooking,
    onHandlePage,
    handleSort,
    onPaging,
    onSearch,
    handleFilter,
    handleReset,
    filterStatus,
    onCheckIn,
    onCheckOut,
    onEditBookingById,
    onAdminBooking,
  };
};

export default useAdmin;
