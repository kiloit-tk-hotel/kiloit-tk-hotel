// request get all booking

import axios from "axios";

const regAllBooking = (params) => {
  return axios.get("api/booking", {
    params: params,
  });
};
const regViewAllBooking = (id) => {
  return axios.get(`api/booking/${id}`);
};

// request get booking by id

//request booking

//request cancel booking
const regCancelBooking = (id) => {
  return axios.post(`api/booking/${id}/cancel`);
};

//request delete booking by id
const regDeleteBooking = (id) => {
  return axios.delete(`api/booking/${id}`);
};

//request update booking
const regUpdateBooking = (id, payload = {}) => {
  return axios.put(`api/booking/${id}`, payload);
};

// request check in

const reqCheckIn = (id) => {
  return axios.post(`api/booking/${id}/check-in`);
};

// request check out

const reqCheckOut = (id) => {
  return axios.post(`api/booking/${id}/check-out`);
};

const regAdminBooking = (payload = {}) => {
  return axios.post("api/booking", payload);
};

const regPaging = (params) => {
  return axios.get("api/booking", {
    params: params,
  });
};

export {
  regAllBooking,
  regViewAllBooking,
  regCancelBooking,
  regDeleteBooking,
  reqCheckIn,
  reqCheckOut,
  regUpdateBooking,
  regAdminBooking,
  regPaging,
};
