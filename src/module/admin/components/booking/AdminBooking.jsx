import { Button, Label, Select, TextInput } from 'flowbite-react'
import { useFormik } from 'formik'
import React, { useEffect } from 'react'
import useRoomType from '../../../room/core/hook'
import { useSelector } from 'react-redux'
import useEndUser from '../../core/hook'
import { useUser } from '../../../user/core/hook'
import Swal from 'sweetalert2'

const AdminBooking = () => {

  const { onGetRoomType } = useRoomType()
  const { onAdminBooking } = useEndUser()
  const {onGetUser} = useUser()
  const { roomTypes } = useSelector((state) => state.roomType)
  const { users} = useSelector((state) => state.user)
  

  const formik = useFormik({
    initialValues: {
      userId: "",
      checkIn: "",
      checkOut: "",
      specialRequest: "",
      bookingDetail: [
        { roomTypeId: "", adult: "", child: "" }
      ]
    },
    onSubmit: (values) => {
      Swal.fire({
        position: "top-end",
        height: "100px",
        icon: "success",
        title: "Add Successed",
        showConfirmButton: false,
        timer: 1500,
      })
        .then(() => {
          onAdminBooking(values);
          formik.resetForm();

        })
        .catch((error) => {
          Swal.fire({
            title: "Error!",
            text: "Failed to update room type",
            icon: "error",
          });
        });
    },
  });

  useEffect(() => {
    onGetRoomType()
    onGetUser()
  }, [])
  return (
    <>
      <div className="p-4">
        <form onSubmit={formik.handleSubmit}>
          <div className="grid gap-5">
            <div>
              <div className="mb-2 block">
                <Label value="userId" />
              </div>
             
              <Select
                    name='userId'
                    onChange={formik.handleChange}
                    value={formik.values.userId}
                    required
                  >
                    {
                      users.map((user) => (
                        <option key={user.id} value={user.id}>
                          {user.id}
                        </option>
                      ))
                    }
                  </Select>

            </div>
            <div>
              <div className="mb-2 block">
                <Label value="checkIn" />
              </div>
              <TextInput name="checkIn" type="date" value={formik.values.checkIn}
                onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="checkOut" />
              </div>
              <TextInput name="checkOut" type="date" value={formik.values.checkOut}
                onChange={formik.handleChange} required />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value="specialRequest" />
              </div>
              <TextInput name="specialRequest" type="text" value={formik.values.specialRequest}
                onChange={formik.handleChange} required />
            </div>
            {formik.values.bookingDetail.map((item, index) => (
              <div key={index}>
                <div className="mb-2 block">
                  <Label value="roomTypeId" />
                  <Select
                    name={`bookingDetail.${index}.roomTypeId`}
                    onChange={formik.handleChange}
                    value={item.roomTypeId}
                    required
                  >
                    {roomTypes.map((roomType) => (
                      <option key={roomType.id} value={roomType.id}>
                        {roomType.id}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="mb-2 block">
                  <Label value="adult" />
                  <TextInput
                    name={`bookingDetail.${index}.adult`}
                    type="text"
                    value={item.adult}
                    onChange={formik.handleChange}
                    required
                  />
                </div>
                <div className="mb-2 block">
                  <Label value="child" />
                  <TextInput
                    name={`bookingDetail.${index}.child`}
                    type="text"
                    value={item.child}
                    onChange={formik.handleChange}
                    required
                  />
                </div>
              </div>
            ))}
          </div>

          <Button outline gradientDuoTone="purpleToBlue" type="submit" className=" mt-4">Submit</Button>
        </form>

      </div >
    </>
  )
}

export default AdminBooking