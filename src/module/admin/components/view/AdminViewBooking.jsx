import { Link, useParams } from "react-router-dom";
import { useEffect } from "react";
import useAdmin from "../../core/hook";
import { useSelector } from "react-redux";
import { Button, Card } from "flowbite-react";
import usePayment from "../../../payment/core/hook";

const AdminViewBooking = () => {

    const { id } = useParams()

    const { onViewBooking } = useAdmin()
    const { viewBooking } = useSelector((state => state.admin))
    const { bookingDetail = [], payment = {} } = viewBooking;


    useEffect(() => {
        onViewBooking(id)
    }, [id]);


    return (
        <div className="">

            <Card className="max-w-md m-10">
                <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white pb-5">
                    {
                        bookingDetail.map((d, index) => {
                            return (
                                <div key={index}>
                                    {
                                        d.roomType.title
                                    }
                                </div>
                            )
                        })
                    }
                </h5>
                <div className="grid grid-cols-2 gap-2">
                    <p className="font-normal text-gray-700 dark:text-gray-400">
                        CheckInAt :
                    </p>
                    <p>{new Date(viewBooking.checkInAt).toLocaleDateString(
                        "en-CA"
                    )}
                    </p>

                    <p className="font-normal text-gray-700 dark:text-gray-400">
                        CheckOutAt :
                    </p>
                    <p>{new Date(viewBooking.checkOutAt).toLocaleDateString(
                        "en-CA"
                    )}
                    </p>

                    <p className="font-normal text-gray-700 dark:text-gray-400">
                        Status :
                    </p>
                    <p>{viewBooking.status}</p>

                    <p className="font-normal text-gray-700 dark:text-gray-400">
                        specialRequest :
                    </p>
                    <p>{viewBooking.specialRequest}</p>

                    <div className="col-span-2">
                        {
                            bookingDetail.map((d, index) => {
                                return (
                                    <div className="grid grid-cols-2 gap-2" key={index}>
                                        <p className="font-normal text-gray-700 dark:text-gray-400">
                                            id :
                                        </p>
                                        <p>{d.id}</p>

                                        <p className="font-normal text-gray-700 dark:text-gray-400">
                                            adult :
                                        </p>
                                        <p>{d.adult}</p>

                                        <p className="font-normal text-gray-700 dark:text-gray-400">
                                            child :
                                        </p>
                                        <p>{d.child}</p>
                                    </div>
                                )
                            })
                        }
                    </div>

                </div>
                <Button>
                    <Link to={`/view/payment/${payment.id}`} className="flex  items-center">
                        Payment
                        <svg className="-mr-1 ml-2 h-4 w-4" fill="currentColor" viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                fillRule="evenodd"
                                d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                                clipRule="evenodd"
                            />
                        </svg>
                    </Link>
                </Button>
            </Card>

        </div>
    );
};

export default AdminViewBooking;
