import { Button, Label, Select, TextInput } from "flowbite-react";
import { useFormik } from "formik";
import React, { useEffect } from "react";
import useAdmin from "../../core/hook";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import useRoomType from "../../../room/core/hook";
import Swal from "sweetalert2";
import {useUser} from "../../../user/core/hook";



const UpdateBooking = () => {
  const { id } = useParams();
  const { onViewBooking, onEditBookingById } = useAdmin();
  const { viewBooking } = useSelector((state) => state.admin);
  const { roomTypes } = useSelector((state) => state.roomType);
  const { users } = useSelector((state) => state.user);
  const { onGetRoomType } = useRoomType();
  const {onGetUser} = useUser()
  const { bookingDetail = [] } = viewBooking;
  const roomTypeId = bookingDetail.map((item) => {
    return item.roomTypeId;
  });

  const adult = bookingDetail.map((item) => {
    return item.adult;
  });

  const child = bookingDetail.map((item) => {
    return item.child;
  });
  const formik = useFormik({
    initialValues: {
      userId: viewBooking.userId,
      checkIn: viewBooking.checkInAt,
      checkOut: viewBooking.checkOutAt,
      specialRequest: viewBooking.specialRequest,
      bookingDetail: [{ roomTypeId: roomTypeId, adult: adult, child: child }],
    },
    enableReinitialize: true,
    onSubmit: (values) => {
      Swal.fire({
        position: "top-end",
        height: "100px",
        icon: "success",
        title: "Add Successed",
        showConfirmButton: false,
        timer: 1500,
      })
        .then(() => {
          onEditBookingById(id, values);
          formik.resetForm();
        })
        .catch((error) => {
          Swal.fire({
            title: "Error!",
            text: "Failed to update room type",
            icon: "error",
          });
        });
    },
  });

  useEffect(() => {
    onViewBooking(id);
    onGetRoomType();
    onGetUser();
  }, [id]);

  return (
    <div className="p-4">
      <form onSubmit={formik.handleSubmit}>
        <div className="grid gap-5">
          <div>
            <Select
              name="userId"
              onChange={formik.handleChange}
              value={formik.values.userId}
              required
            >
              {users.map((user) => (
                <option key={user.id} value={user.id}>
                  {user.id}
                </option>
              ))}
            </Select>
            <div className="mb-2 block">
              <Label value="checkIn" />
            </div>
            <TextInput
              name="checkIn"
              type="date"
              value={formik.values.checkIn}
              onChange={formik.handleChange}
              required
            />
            <div className="mb-2 block">
              <Label value="checkOut" />
            </div>
            <TextInput
              name="checkOut"
              type="date"
              value={formik.values.checkOut}
              onChange={formik.handleChange}
              required
            />
            <div className="mb-2 block">
              <Label value="specialRequest" />
            </div>
            <TextInput
              name="specialRequest"
              type="text"
              value={formik.values.specialRequest}
              onChange={formik.handleChange}
              required
            />
          </div>
          {formik.values.bookingDetail.map((item, index) => (
            <div key={index}>
              <div className="mb-2 block">
                <Label value="roomTypeId" />
                <Select
                  name={`bookingDetail.${index}.roomTypeId`}
                  onChange={formik.handleChange}
                  value={item.roomTypeId}
                  required
                >
                  {roomTypes.map((roomType) => (
                    <option key={roomType.id} value={roomType.id}>
                      {roomType.id}
                    </option>
                  ))}
                </Select>
              </div>
              <div className="mb-2 block">
                <Label value="adult" />
                <TextInput
                  name={`bookingDetail.${index}.adult`}
                  type="text"
                  value={item.adult}
                  onChange={formik.handleChange}
                  required
                />
              </div>
              <div className="mb-2 block">
                <Label value="child" />
                <TextInput
                  name={`bookingDetail.${index}.child`}
                  type="text"
                  value={item.child}
                  onChange={formik.handleChange}
                  required
                />
              </div>
            </div>
          ))}
        </div>

        <Button
          outline
          gradientDuoTone="purpleToBlue"
          type="submit"
          className=" mt-4"
        >
          Submit
        </Button>
      </form>
    </div>
  );
};

export default UpdateBooking;
