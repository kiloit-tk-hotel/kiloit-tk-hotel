import "./App.css";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useAuth } from "./module/auth/core/hook";
import AppRouter from "./module/routes/AppRouter";

function App() {
  const { token } = useSelector((state) => state.auth);
  const { onGetProfile } = useAuth();
  useEffect(() => {
    token && onGetProfile();
  }, [token]);
  return <AppRouter />;
}

export default App;
