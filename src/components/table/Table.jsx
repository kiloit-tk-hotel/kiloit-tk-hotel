import React, { useEffect } from "react";

import Swal from "sweetalert2";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Badge, Button, Checkbox, Table } from "flowbite-react";
import useRoomType from "./core/hook";
import Component from "../../components/spinner";

import { TiArrowSortedDown } from "react-icons/ti";

const Table = () => {


  const dispatch = useDispatch();
  const { onGetRoomType } = useRoomType();
  const { onDeleteRoomType } = useRoomType();

  const { roomTypes, loading } = useSelector(
    (state) => state.roomType
  );

  const handleDelete = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        onDeleteRoomType(id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  };

  useEffect(() => {
    onGetRoomType();
  }, []);

  return (
    <div className="shadow p-5">
      {loading ? (
        <>
          <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
            <Component />
          </div>
        </>
      ) : (
        <>
          <div className="flex justify-between items-center mb-5">
            <span className="text-md font-medium">Room Type</span>
            <Link to="/room/add">
              <Button outline gradientDuoTone="purpleToBlue">
                Add Room
              </Button>
            </Link>
          </div>


          <div className="overflow-x-auto">
            <Table hoverable>
              <Table.Head>
                <Table.HeadCell>Checkbox</Table.HeadCell>
                <Table.HeadCell
                  className=""
                >
                  <div className="flex gap-2 items-center">
                    <span>Room Description</span>
                    <span>
                      <TiArrowSortedDown />
                    </span>
                  </div>
                </Table.HeadCell>
                <Table.HeadCell
                  className=""
                >
                  <div className="flex gap-2 items-center">
                    <span>Room Title</span>
                    <span>
                      <TiArrowSortedDown />
                    </span>
                  </div>
                </Table.HeadCell>

                <Table.HeadCell className="">
                  <div className="flex gap-2 items-center">
                    <span>Bed</span>
                    <span>
                      <TiArrowSortedDown />
                    </span>
                  </div>
                </Table.HeadCell>

                <Table.HeadCell>Room Amenity</Table.HeadCell>
                <Table.HeadCell
                  className=""
                >
                  <div className="flex gap-2 items-center">
                    <span>Price</span>
                    <span>
                      <TiArrowSortedDown />
                    </span>
                  </div>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">View</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Delete</span>
                </Table.HeadCell>
                <Table.HeadCell>
                  <span className="sr-only">Update</span>
                </Table.HeadCell>
              </Table.Head>
              <Table.Body className="divide-y">
                {roomTypes.map((room) => {
                  return (
                    <Table.Row
                      key={room.id}
                      className="bg-white dark:border-gray-700 dark:bg-gray-800"
                    >
                      <Table.Cell className="p-4">
                        <Checkbox />
                      </Table.Cell>
                      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                        {room.description}
                      </Table.Cell>
                      <Table.Cell>{room.title}</Table.Cell>
                      <Table.Cell>{room.bed}</Table.Cell>
                      <Table.Cell>{room.amenity}</Table.Cell>
                      <Table.Cell>${room.price}</Table.Cell>
                      <Table.Cell>
                        <Link
                          to={`/viewRoom/${room.id}`}
                          className="font-medium text-cyan-600 hover:underline dark:text-sky-500"
                        >
                          <Badge
                            color="success"
                            className="flex items-center justify-center"
                          >
                            View
                          </Badge>
                        </Link>
                      </Table.Cell>
                      <Table.Cell
                        onClick={() => handleDelete(room.id)}
                        className="font-medium text-red-600 hover:underline decor  cursor-pointer"
                      >
                        <Badge
                          color="failure"
                          className="flex items-center justify-center"
                        >
                          Delete
                        </Badge>
                      </Table.Cell>
                      <Table.Cell className="font-medium text-green-500-600 hover:underline decor cursor-pointer ">
                        <Link to={`/editRoomType/${room.id}`}>
                          <Badge
                            color="purple"
                            size="md"
                            className="flex items-center justify-center"
                          >
                            Edit
                          </Badge>
                        </Link>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
              </Table.Body>
            </Table>
          </div>
        </>
      )}
    </div>
  );
};

export default Table;
