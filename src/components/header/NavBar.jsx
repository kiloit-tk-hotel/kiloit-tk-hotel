import React from "react";
import { Avatar, Dropdown, Navbar } from "flowbite-react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const NavBar = () => {

  const { profile } = useSelector(state => state.auth)

  return (
    <Navbar fluid rounded className="shadow w-full max-w-none">
      <Navbar.Brand >
        <span className="self-center ml-5 whitespace-nowrap text-xl font-semibold dark:text-white">
          <Link to='/'>
            Hotel Dashboard
          </Link>
        </span>
      </Navbar.Brand>
      <div className="flex md:order-2">
        <Dropdown
          arrowIcon={false}
          inline
          label={<Avatar alt="User settings" img={profile.avatar} rounded />}
        >
          <Dropdown.Header>
            <span className="block text-sm">{profile.name}</span>
            <span className="block truncate text-sm font-medium">
              {profile.email}
            </span>
          </Dropdown.Header>
          <Link to='/'>
            <Dropdown.Item>Dashboard</Dropdown.Item>
          </Link>
          <Link to='/profile'>
            <Dropdown.Item>Profile</Dropdown.Item>
          </Link>
          <Dropdown.Divider />
          <Dropdown.Item>
            <Link to='/resetPassword'>
              Reset Password
            </Link>
          </Dropdown.Item>
          <Dropdown.Item onClick={() => {
            localStorage.removeItem('token')
            window.location.reload()
          }}>Sign out</Dropdown.Item>
        </Dropdown>
      </div>
    </Navbar>
  );
};

export default NavBar;
