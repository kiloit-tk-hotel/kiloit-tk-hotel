import { Button, Spinner } from "flowbite-react";

function Component() {
  return (
    <div className="flex flex-row gap-3">
      <Button color="gray">
        <Spinner aria-label="Alternate spinner button example" size="lg" />
      </Button>
    </div>
  );
}

export default Component;
