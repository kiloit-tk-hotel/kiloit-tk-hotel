import React from "react";
import { Sidebar } from "flowbite-react";
import { Link } from "react-router-dom";
import { MdDashboardCustomize } from "react-icons/md";
import { FaRegUserCircle } from "react-icons/fa";
import { RiGameFill } from "react-icons/ri";
import { MdBedroomParent } from "react-icons/md";
import { CgProfile } from "react-icons/cg";
import { RiArrowLeftDoubleFill } from "react-icons/ri";
import { LiaBookmark } from "react-icons/lia";
import { RiAdminFill } from "react-icons/ri";
import { MdPayment } from "react-icons/md";



const SideBar = ({ open, setOpen }) => {
  return (
    <div className={`fixed z-10 flex flex-col ${open ? "w-64" : "w-20"}  h-full bg-gray-100`}>
      <Sidebar className="shadow w-auto relative">
        <div
          className={`absolute top-5 transition-all ease-in-out  ${open ? "-right-3" : "-right-2 rotate-180"
            }`}
        >
          <RiArrowLeftDoubleFill
            size={27}
            onClick={() => setOpen((prev) => !prev)}
          />
        </div>
        <Sidebar.Items>
          <Sidebar.ItemGroup className="flex flex-col">
            <Link
              to="/"
              className="hover:bg-gray-200 rounded-md p-2 flex items-center "
            >
              <MdDashboardCustomize className="inline-block mr-2 text-[20px]" />
              <p className={`${open ? "block" : "hidden"}`}>DashBoard</p>
            </Link>

            <Link
              to="/user"
              className="hover:bg-gray-200 rounded-md p-2 flex items-center "
            >
              <FaRegUserCircle className="inline-block mr-2 text-[20px]" />
              <p className={` ${open ? "block" : "hidden"}`}>User</p>
            </Link>

            <Link
              to="/role"
              className="hover:bg-gray-200 rounded-md p-3 flex items-center"
            >
              <RiGameFill className="inline-block mr-2 text-[20px]" />
              <p className={`${open ? "block" : "hidden"}`}>Role</p>
            </Link>

            <Link
              to="/roomType"
              className="hover:bg-gray-200 rounded-md p-3 flex items-center"
            >
              <MdBedroomParent className="inline-block mr-2 text-[20px]" />
              <p className={`${open ? "block" : "hidden"}`}>Room Type</p>
            </Link>

            <Link
              to="/roomManagement"
              className="hover:bg-gray-200 rounded-md p-3 flex items-center"
            >
              <CgProfile className="inline-block mr-2 text-[20px]" />
              <p className={`${open ? "block" : "hidden"}`}>Room Management</p>
            </Link>


            <Link
              to="/displayUserBooking"
              className="hover:bg-gray-200 rounded-md p-3 flex items-center"
            >
              <LiaBookmark className="inline-block mr-2 text-[20px]" />
              <p className={`${open ? 'block' : 'hidden'}`}>End-User Management</p>
            </Link>

            <Link
              to="/getAllBooking"
              className="hover:bg-gray-200 rounded-md p-3 flex items-center"
            >
              <RiAdminFill className="inline-block mr-2 text-[20px]" />
              <p className={`${open ? 'block' : 'hidden'}`}> Admin Management</p>
            </Link>


          </Sidebar.ItemGroup>
        </Sidebar.Items>
      </Sidebar>
    </div>
  );
};

export default SideBar;
